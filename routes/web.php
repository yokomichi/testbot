<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::post('/webhook', 'ApiController@webhook');

/**
 * SPA(mypageのみ)
 */
Route::group(['middleware' => ['auth']], function () {
    Route::get('/mypage', function () {return view('spa');});
    Route::get('/mypage/order', function () {return view('spa');});
    Route::get('/mypage/autoship', function () {return view('spa');});
    Route::get('/mypage/info', function () {return view('spa');});
    Route::get('/mypage/autoship/nextdelivery', function () {return view('spa');});
    Route::get('/mypage/payment', function () {return view('spa');});
    Route::get('/mypage/autoship/purchase', function () {return view('spa');});
    Route::get('/mypage/autoship/purchase/{item_id}', function () {return view('spa');});
    Route::get('/mypage/contract', function () {return view('spa');});
    Route::get('/mypage/order/history', function () {return view('spa');});
    Route::get('/mypage/invoice', function () {return view('spa');});
    Route::get('/mypage/payment', function () {return view('spa');});
    Route::get('/mypage/autoship_list', function () {return view('spa');});
    Route::get('/mypage/autoship_detail/{item_id}', function () {return view('spa');});
    Route::get('/mypage/autoship/order/{item_id}', function () {return view('spa');});
    Route::get('/mypage/password', function () {return view('spa');});
    Route::get('/mypage/faq', function () {return view('spa');});
    Route::get('/mypage/notes', function () {return view('spa');});
    // Route::get('/mypage/campaign/friend', function () {return view('spa');});
    // Route::get('/mypage/campaign/sticker', function () {return view('spa');});
    //次回お届け内容取得
    Route::post('/axios/autoship/order/search', 'AxiosController@getNextAutoshipOrder');
    //次回お届け内容変更
    Route::post('/axios/savenextorder', 'AxiosController@SaveNextOrder');
    // 定期商品一覧取得
    Route::post('/axios/autoship_list', 'AxiosController@getAutoshipList');
    // 定期商品詳細取得
    Route::post('/axios/autoship', 'AxiosController@getAutoshipDetail');
    // 定期商品購入保存API
    Route::post('/axios/save/autoship', 'AxiosController@SaveAutoship');
    // 定期商品取得API
    Route::post('/axios/getautoship', 'AxiosController@getAutoship');
    // User取得API
    Route::post('/axios/savepayment', 'AxiosController@savePayment');
    // ユーザ定期購入詳細
    Route::post('/axios/getautoshipdetail', 'AxiosController@getPurchaseAutoshipDetail');
    // ユーザ定期購入詳細
    Route::post('/axios/save/autoshipdetail', 'AxiosController@savePurchaseAutoshipDetail');
    // ユーザ定期購入詳細
    Route::post('/axios/skip/autoship', 'AxiosController@skipAutoship');
});
// User取得API
Route::post('/axios/getmember', 'AxiosController@getMember');
// User取得API
Route::post('/axios/getauth', 'AxiosController@getauth');

// Session取得API
Route::post('/axios/getsession', 'AxiosController@getsession');
// Config取得API
Route::post('/axios/getconfig', 'AxiosController@getConfig');
// 登録メール送信API
Route::post('/axios/mail/register', 'AxiosController@mailRegister');
// 仮登録内容取得API
Route::post('/axios/get/member_temporary', 'AxiosController@getMemberTemporary');
// 仮登録内容保存API
Route::post('/axios/save/member_temporary', 'AxiosController@saveMemberTemporary');
// 登録内容保存API
Route::post('/axios/save/member', 'AxiosController@saveMember');
// 商品購入保存API
Route::post('/axios/save/order', 'AxiosController@SaveOrder');


// forget取得API
Route::post('/axios/forget', 'AxiosController@getforget');
// User取得API
Route::post('/axios/User', 'AxiosController@getUser');
// User取得API
Route::post('/axios/password/edit', 'AxiosController@chengedPassword');
// 商品取得
Route::post('/axios/product_list', 'AxiosController@getProductList');
// 商品詳細取得
Route::post('/axios/product', 'AxiosController@getProductDetail');
// カート追加
Route::post('/axios/addtocart', 'AxiosController@AddToCart');
// カート個数取得
Route::post('/axios/getitemcount', 'AxiosController@getItemCount');
// ユーザーカート取得
Route::post('/axios/getuserproduct', 'AxiosController@getUserProduct');
// カート保存
Route::post('/axios/saveproduct', 'AxiosController@SaveProduct');
// 購入履歴取得
Route::post('/axios/getproducthistory', 'AxiosController@getProductHistory');
Route::post('/axios/getproducthistorydetail', 'AxiosController@getProductHistoryDetail');
//ご契約内容の照会 変更
Route::post('/axios/member/edit', 'AxiosController@editMember');
// カードトークン保存API
Route::post('/axios/save/token', 'AxiosController@saveToken');
// カートのセッション紐付けAPI
Route::post('/axios/peggingcart', 'AxiosController@peggingCart');

// サイドメニュー取得API
Route::post('/axios/getsidemenu', 'AxiosController@getSidemenu');






Route::get('/cart', function () {return view('spa');});
Route::get('/complete', function () {return view('spa');});
Route::get('/confirm', function () {return view('spa');});
Route::get('/payment', function () {return view('spa');});
Route::get('/product_list', function () {return view('spa');});
Route::get('/product_detail/{item_id}', function () {return view('spa');});
Route::get('/shopping/login', function () {return view('spa');})->name('login_default');
Route::get('/order', function () {return view('spa');});

/**
 * LP
*/
Route::get('/register', function () {return view('spa');});
Route::get('/forget', function () {return view('spa');});


Route::get('/', function () {return view('lp.top');});
Route::get('/policy', function () {return view('lp.policy');});
Route::get('/terms', function () {return view('lp.terms');});
Route::get('/company', function () {return view('lp.company');});
Route::get('/transactionact', function () {return view('lp.transactionact');});
Route::get('/comingsoon', function () {return view('lp.comingsoon');});
Route::get('/info', function () {return view('lp.info');});
Route::get('/contact', function () {return view('lp.contact');});


Route::get('/login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('/login', 'Auth\LoginController@login');
Route::post('/logout', 'Auth\LoginController@logout');


Route::get('/sitemap', 'SiteMapController@sitemap');