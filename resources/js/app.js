import Vue from 'vue';
import Vuex from 'vuex'
import VueRouter from 'vue-router';
require('./bootstrap');
import axios from 'axios';
// import 'es6-promise/auto'

window.Vue = require('vue');
Vue.use(VueRouter);
Vue.use(Vuex);
import Common from './views/js/common';
import Env from './views/js/env';
import App from './App.vue';
const eventHub = global.eventHub = new Vue();
Vue.mixin(Common);
Vue.mixin(Env);
const router = new VueRouter({
    mode: 'history',
    routes: [
        // mypageトップ
        { path: '/mypage', component: App },
        // 会員登録
        { path: '/register', component: App },
        // パスワード忘れ
        { path: '/forget', component: App },
        // パスワード変更
        { path: '/mypage/password', component: App },
        // 契約内容変更
        { path: '/mypage/contract', component: App },
        // 次回お届け内容
        { path: '/mypage/autoship/nextdelivery', component: App },
        // ユーザ購入定期
        { path: '/mypage/autoship/purchase', component: App },
        // ユーザ購入定期詳細
        { path: '/mypage/autoship/purchase/:item_id', component: App },
        // カート
        { path: '/cart', component: App },
        // 注文
        { path: '/order', component: App },
        // 注文画面ログイン
        { path: '/shopping/login', component: App },
        // 商品一覧
        { path: '/product_list', component: App },
        // 商品詳細
        { path: '/product_detail/:item_id', component: App },
        // 購入履歴
        { path: '/mypage/order/history', component: App },
        // 支払方法
        { path: '/mypage/payment', component: App },
        // 定期商品一覧
        { path: '/mypage/autoship_list', component: App },
        // 定期商品詳細
        { path: '/mypage/autoship_detail/:item_id', component: App },
        // 定期商品注文
        {path: '/mypage/autoship/order/:item_id',component: App},
    ]
});
/**
* Next, we will create a fresh Vue application instance and attach it to
* the page. Then, you may begin adding components to this application
* or customize the JavaScript scaffolding to fit your unique needs.
*/
const store = new Vuex.Store({
    state: {
        url_component:{
            '/mypage': 'Home',
            '/register': 'Register',
            '/forget': 'Forget',
            '/mypage/password': 'Password',
            '/mypage/contract': 'MemberContract',
            '/mypage/autoship/nextdelivery':'NextDelivery',
            '/mypage/autoship/purchase':'AutoshipPurchaseList',
            '/mypage/autoship/purchase/:item_id':'AutoshipPurchaseDetail',
            '/mypage/payment':'Payment',
            '/cart': 'Cart',
            '/order': 'OrderParent',
            '/shopping/login': 'ShoppingLogin',
            '/product_list':'ProductList',
            '/product_detail/:item_id':'ProductDetail',
            '/mypage/order/history' : 'OrderHistory',
            '/mypage/autoship_list':'AutoshipList',
            '/mypage/autoship_detail/:item_id':'AutoshipDetail',
            '/mypage/autoship/order/:item_id':'AutoshipOrderParent',
        },
        breadcrumblist:{
            '/mypage': {name:'マイページ'},
            '/register':{name:'会員登録'} ,
            '/forget': {name:'パスワードを忘れた方'},
            '/mypage/password': {name:'パスワードの変更'},
            '/mypage/contract': {name:'ご契約内容の照会'},
            '/mypage/autoship/nextdelivery': {name:'次回お届け内容の確認'},
            '/mypage/autoship/purchase': {name:'定期契約済みの商品一覧'},
            '/mypage/autoship/purchase/:item_id': {name:'定期契約済みの商品詳細'},
            '/mypage/payment': {name:'定期お支払い方法'},
            '/cart': {name:'ショッピングカート'},
            '/order': {name:'ご購入手続き'},
            '/shopping/login':  {name:'ログイン・ご注文'},
            '/product_list':{name:'商品一覧'},
            '/product_detail/:item_id':{name:'商品一覧',path:'/product_list',name2:'商品詳細'},
            '/mypage/order/history' : {name:'購入履歴'},
            '/mypage/autoship_list':{name:'定期購入可能な商品一覧'},
            '/mypage/autoship_detail/:item_id': { name: '定期購入可能な商品一覧', path: '/autoship_list', name2: '定期購入可能な商品詳細' },
            '/mypage/autoship/order/:item_id': {name:'定期契約手続き'},
        },

        progressBar: {
            processing: false
        },
        product: [],
        config: [],
        autocomplete:[],
        current_page: 1,
        last_page: 1,
        total: 1,
        from: 0,
        to: 0,
        count: 9, // 一覧に幾つ表示するかの設定
        orderby: 'item_id',
        sort: 'asc',
        request:{
            'word': '',
        },
        auth: false,
        session: '',
        ParentLoader: false,
        MailModal: false,
        item_count:0,
        member_id:'',
        username: '',
        addCartItem: [],
        success_message: '',
        error_message: '',
        success_stack: [],
        error_stack: [],
        OrderLinkFlag: false,
        Breadcrumb:[],
        manage_url: '',
    },
    getters: {
        getSuccessMassage: state => {
            return state.success_message;
        },
        getErrorMassage: state => {
            return state.error_message;
        }
    },
    mutations: {
        getItemCount: function(state) {
            axios.post('/axios/getitemcount', {session:state.session}).then(function (res) {
                state.item_count = res.data;
            });
        },
        getAutoComplete: function(state) {
            axios.post('/axios/autocomplete/product').then(function (res) {
                state.autocomplete = res.data;
            });
        },
        setCount: function (state, v) {
            state.count = v;
        },
        setWord: function (state, v) {
            state.request.word = v;
        },
        start_process({ progressBar }) {
            progressBar.processing = true;
        },
        end_process({ progressBar }) {
            const dom = document.querySelector(".progress-bar");
            if (dom !== null) {
                dom.style.animationPlayState = "paused";
                dom.style.animation = "none";
                dom.style.width = "100%";
            }
            setTimeout(() => {
                progressBar.processing = false;
            }, 300);
        }
    },
    actions: {
        getItemCountAction: function(ctx) {
            ctx.commit('getItemCount')
        },
        setCountAction: function(ctx, v) {
            ctx.commit('setCount', v)
        },
        setWordAction(ctx, v) {
            ctx.commit('setWord', v)
        },
        getAutoCompleteAction(ctx) {
            ctx.commit('getAutoComplete');
        }
    }
})

export default store
const app_element = document.getElementById('app');
if (app_element) {
    const app = new Vue({
        router,
        store,
        el: '#app',
        methods: {
            handler(event) {
                window.scrollTo(0, 0);
            }
        },
        created() {
            window.addEventListener("beforeunload", this.handler);
        },
        destroyed () {
            window.removeEventListener("beforeunload", this.handler)
        },
        scrollBehavior (to, from, savedPosition) {
            if (savedPosition) {
                return savedPosition;
            } else {
                return { x: 0, y: 0 };
            }
        }
    });
}

import SidemenuComponent from './components/LpSidemenu.vue';
const sidemenu_element = document.getElementById('sidemenu');
if (sidemenu_element) {
    new Vue({
        el: '#sidemenu',
        store,
        router,
        components: {SidemenuComponent},
    });
}
