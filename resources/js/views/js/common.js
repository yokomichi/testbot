const autocomplete_date = '-'; // 日付フォーマットのための文字列
const autocomplete_post = '-'; // 郵便番号フォーマットのための文字列

export default {
    data: function () {
        return {
            validate_message: {
                Require:'必須です',
                RequireOnce:':value:の場合、必須です',
                Katakana:'カタカナで入力して下さい',
                HankakuKatakana:'半角カタカナで入力して下さい',
                Num:'半角数字で入力して下さい',
                Alpha:'半角ローマ字で入力して下さい',
                NumAlpha: '半角英数字で入力して下さい',
                NumDash:'半角数字とハイフンで入力して下さい',
                Mail:'正しいメールアドレスを入力して下さい',
                Date:'正しい日付を入力して下さい',
                Post: '例:000-0000のように半角数字で入力してください',
                PostNotExist: '存在しない郵便番号です',
                MemberNotExist: '存在しない会員番号です',
                Length: ':value:文字以内で入力して下さい',
                LengthOver: ':value:文字以上で入力して下さい',
                Count: ':count:以上で入力して下さい',
            }
        }
    },
    created() {
        var arr = new Array();
        if(document.cookie != ''){
            var tmp = document.cookie.split('; ');
            for(var i=0;i<tmp.length;i++){
                var data = tmp[i].split('=');
                arr[data[0]] = decodeURIComponent(data[1]);
            }
        }
        if (arr['wp_session'] == null) {
            this.$store.state.error_message = 'お客様の情報が確認できませんでした。 1秒後にショッピング画面へ遷移します。';
            setTimeout(this.JumpShopping, 1000);
        }
    },
    methods: {
        /* axiosでcatch処理 */
        AuthCheck: function(error) {
            var csrf = document.querySelector('meta[name="csrf-token"]').getAttribute('content');
            if (error.response.status !== 419) {
                // // フォームの生成
                // var form = document.createElement("form");
                // form.setAttribute("action", '/logout');
                // form.setAttribute("method", "post");
                // form.style.display = "none";
                // document.body.appendChild(form);
                // var input = document.createElement('input');
                // input.setAttribute('type', 'hidden');
                // input.setAttribute('name', '_token');
                // input.setAttribute('value', csrf);
                // form.appendChild(input);
                // form.submit();
            } else if (error.response.status === 419) {
                location.href = '/login';
            }
        },
        /* カスタムキー(現在はTab)で前のinputへ移動 */
        prevFocus: function(event) {
            const elements = document.getElementsByClassName('for__focus');
            const index = [].findIndex.call(elements, e => e === event.target);
            if (elements[index - 1] !== undefined) {
                elements[index - 1].focus();
            } else {
                elements[Object.keys(elements).length - 1].focus();
            }
        },
        /* カスタムキー(現在はEnter)で次のinputへ移動 */
        nextFocus: function(event) {
            if (event.keyCode !== 13) return
            const elements = document.getElementsByClassName('for__focus');
            const index = [].findIndex.call(elements, e => e === event.target);
            if (elements[index + 1] !== undefined) {
                elements[index + 1].focus();
            } else {
                elements[0].focus();
            }
        },
        /* 日付の自動補完 */
        FormatDate: function(n = '', o = '') {
            if (n  == null || o == null) return n;
            var date = new Date();
            if (n.length === 1 && n == '7') return this.dateToStr24HPad0(new Date(date.getFullYear(), date.getMonth(), 1)); // 今月初日の日付
            if (n.length === 1 && n == '8') return this.dateToStr24HPad0(date); // 今日の日付
            if (n.length === 1 && n == '9') return this.dateToStr24HPad0(new Date(date.getFullYear(), date.getMonth()+1, 0)); // 今月末日の日付
            var date = n,
                split_n = n.match(autocomplete_date) ? n.split(autocomplete_date) : 1,
                split_o = o.match(autocomplete_date) ? o.split(autocomplete_date) : 1;
            switch (n.length) {
                case 4:
                    if (split_o.length === 2) {
                        date = n;
                    } else {
                        date = n + autocomplete_date;
                    }
                    break;
                case 5:
                    if (split_n.length === 1) {
                        if (Number(n.substr(4, 1)) > 1) {
                            date = n.substr(0, 4) + autocomplete_date + '0' + n.substr(4, 1);
                        } else {
                            date = n.substr(0, 4) + autocomplete_date + n.substr(4, 1);
                        }
                    }
                    break;
                case 6:
                    if (Number(n.substr(-1)) > 1) {
                        date = o + '0' + n.substr(-1) + autocomplete_date;
                    }
                    break;
                case 7:
                    if (split_o.length === 3) {
                        date = n;
                    } else {
                        date = n + autocomplete_date;
                    }
                    break;
                case 8:
                    if (split_n.length === 2) {
                        if (Number(n.substr(8, 1)) > 3) {
                            date = n.substr(0, 4) + autocomplete_date + n.substr(4, 2) + autocomplete_date + '0' + n.substr(8, 1);
                        } else {
                            date = n.substr(0, 4) + autocomplete_date + n.substr(4, 2) + autocomplete_date + n.substr(8, 1);
                        }
                    }
                    break;
                case 9:
                    if (Number(n.substr(-1)) > 3) {
                        date = o + '0' + n.substr(-1);
                    }
                    break;
            }
            return date;
        },
        /* 郵便番号の自動補完 */
        FormatPost: function(n = '', o = '') {
            if (n  == null || o == null) return n;
            var post = n,
                split_o = o.match(autocomplete_post) ? o.split(autocomplete_post) : 1;
            switch (n.length) {
                case 3:
                    if (split_o.length === 2) {
                        post = n;
                    } else {
                        post = n + autocomplete_post;
                    }
                    break;
            }
            return post;
        },
        /* 日付置換 */
        dateToStr24HPad0: function(date, format) {
            if (!format) format = 'YYYY' + autocomplete_date + 'MM' + autocomplete_date + 'DD';
            // フォーマット文字列内のキーワードを日付に置換する
            format = format.replace(/YYYY/g, date.getFullYear());
            format = format.replace(/MM/g, ('0' + (date.getMonth() + 1)).slice(-2));
            format = format.replace(/DD/g, ('0' + date.getDate()).slice(-2));
            return format;
        },
        /* カスタムキー(現在はTab)で前のinputへ移動 */
        isType: function(x) {
            return (x != x)? "NaN": (x === Infinity || x === -Infinity)? "Infinity": Object.prototype.toString.call(x).slice(8, -1);
        },
        checkPost: function(v) {
            if (!v || v === '') return false;
            return  /^[0-9]{3}-[0-9]{4}/.test(v);
        },
        checkNum: function(v) {
            if (!v || v === '') return false;
            return  /^[-]?[0-9]*$/.test(v);
        },
        checkNumDash: function(v) {
            if (!v || v === '') return false;
            return  /^[0-9\-]+$/.test(v);
        },
        checkRequire: function(v) {
            if (this.isType(v) === 'Date') v = this.dateToStr24HPad0(v);
            if (!v || v === '') return false;
            return /.+/.test(v);
        },
        checkAlpha: function(v) {
            if (!v || v === '') return false;
            return /^[a-zA-Z]+$/.test(v);
        },
        checkNumAlpha: function(v) {
            if (!v || v === '') return false;
            return /^[a-zA-Z0-9]+$/.test(v);
        },
        checkMail: function(v) {
            if (!v || v === '') return false;
            return /^[A-Za-z0-9]{1}[A-Za-z0-9_.-]*@{1}[A-Za-z0-9_.-]{1,}\.[A-Za-z0-9]{1,}$/.test(v);
        },
        checkKatakana: function(v) {
            if (!v || v === '') return false;
            return /^[ァ-ヾ〜ー−]+$/u.test(v);
        },
        checkHankakuKatakana: function(v) {
            if (!v || v === '') return false;
            return /^[ｦ-ﾟ]+$/u.test(v);
        },
        checkDate: function(v) {
            if (!v || v === '' || v.length < 10) return false;
            if (this.isType(v) === 'Date') v = this.dateToStr24HPad0(v);
            var date_array = v.split(autocomplete_date),
            y = Number(date_array[0]),
            m = Number(date_array[1]),
            d = Number(date_array[2]);
            var date = new Date(y, m - 1, d);
            var month = date.getMonth() + 1;
            return m == month ? true : false;
        },
        JumpShoppingLogin: function() {
            location.href = '/shopping/login';
        },
        JumpShopping: function () {
            location.href = this.WORDPRESS_URL + '/product_list/?category=all_item';
        },

    }
}