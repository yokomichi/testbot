export const request = {
    company_name: '',
    first_name: '',
    last_name: '',
    first_kana_name: '',
    last_kana_name: '',
    post: '',
    pref: 1,
    city: '',
    addr: '',
    building: '',
    mail: '',
    phone: '',
}

export const checkArray = {
    company_name: ['RequireOnce'],
    first_name: ['Require'],
    last_name: ['Require'],
    first_kana_name: ['Require', 'Katakana'],
    last_kana_name: ['Require', 'Katakana'],
    post: ['Require', 'Post'],
    city: ['Require'],
    addr: ['Require'],
    mail: ['Require', 'Mail'],
    phone: ['Require', 'Num'],
}
