@extends('lp.layouts.master')

@section('title')
次亜塩素酸水の Guard Water  (ガードウォーター)　本店 - ログイン
@endsection

@section('breadcrumb')
    <div class="breadcrumb__wrapper">
        <ul class="breadcrumb__list">
            <li class="breadcrumb-home"><a href="/">TOP</a></li>
            <li>ログイン</li>
        </ul>
    </div><!--breadcrumb__wrapper end-->
@endsection

@section('content')
    <h2 class="headline">ログイン</h2>
    <form method="POST" action="{{ route('login') }}">
        @csrf
        <div class="article__area">
            <dl class="dl__line dotted">
                <div>
                    <dt class="required">メールアドレス</dt>
                    <dd>
                        <input id="mail" type="text" class="form-control @error('mail') is-invalid @enderror" name="mail" value="{{ old('mail') }}" required autocomplete="mail" autofocus placeholder="example@example.com">
                        <br/>
                        @error('mail')
                            <span class="invalid-feedback element__color-red" role="alert">
                                <strong>{{ __($message) }}</strong>
                            </span>
                        @enderror
                    </dd>
                </div>
                <div>
                    <dt class="required">パスワード</dt>
                    <dd>
                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password" placeholder="半角英数字">
                        <br/>
                        @error('password')
                            <span class="invalid-feedback element__color-red" role="alert">
                                <strong>{{ __($message) }}</strong>
                            </span>
                        @enderror
                    </dd>
                </div>
                <div>
                    <ul class="list__asterisk-red">
                        <li>
                            パスワードの取得またはパスワードを忘れた方はこちらからパスワードの再発行を行ってください。<br>
                            <a href="/forget" class="nav">パスワードの再発行</a>
                        </li>
                    </ul>
                </div>
                <div class="btn__wrapper">
                    <!--<a class="btn register" href="/register">新規登録</a>-->
                    <button type="submit" class="btn next">ログイン</button>
                </div>
            </dl>
            <div>
                @if (Route::has('password.request'))
                    <a class="btn btn-link" href="{{ route('password.request') }}">
                        {{ __('Forgot Your Password?') }}
                    </a>
                @endif
            </div>
        </div>
    </form>
@endsection
