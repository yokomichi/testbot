@extends('lp.layouts.master')

@section('title')
次亜塩素酸水の Guard Water  (ガードウォーター)　本店 - ページが見つかりませんでした
@endsection

@section('breadcrumb')
<!--<div class="breadcrumb__wrapper">
    <ul class="breadcrumb__list">
        <li class="breadcrumb-home">TOP</li>
    </ul>
</div>--><!--breadcrumb__wrapper end-->
@endsection

@section('content')
    <h2 class="headline">ページが見つかりませんでした</h2>
    <div class="article__area">
        <div class="notfound__wrapper">
            <span class="notfound__headline">404</span>
            <span class="notfound__headline-title">Not Found</span>
            <div class="notfound__area">
                指定されたページは存在しないか、または移動した可能性があります。<br>
                お手数ですが、TOPページより再度アクセスをお願いいたします。
            </div>
        </div>
        <div class="btn__wrapper">
            <a class="btn" href="{{env('WORDPRESS_URL')}}">HOME</a>
        </div>
    </div><!--article__area end-->
@endsection
