<!doctype html>
<html>
<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-EVMGNGR325"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'G-EVMGNGR325');
    </script>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="format-detection" content="telephone=no">
    <meta name="description" content="次亜塩素酸水販売のDEMO SPIDER（グランブルー）" />
    <meta name="keywords" content="DEMO SPIDER,グランブルー,水,次亜塩素酸水,Guard Water,ガードウォーター,噴霧器">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>次亜塩素酸水の Guard Water  (ガードウォーター)　本店</title>
    <!--css-->
    <link rel="stylesheet" href="{{ asset('css/pc.bundle.css') }}" media="print, screen and (min-width: 1024px)">
    <link rel="stylesheet" href="{{ asset('css/sp.bundle.css') }}" media="screen and (max-width: 1023px)">
    <link href="{{ asset('css/print.css') }}" media="print" rel="stylesheet" type="text/css">
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-MJGFJN3');</script>
    <!-- End Google Tag Manager -->
</head>

<body>
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MJGFJN3"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    <div class="wrapper">
        <div id="app">
            <router-view></router-view>
        </div>
        <!-- link to the SqPaymentForm library -->
        <script type="text/javascript" src="{{ env('SQ_PAYMENT_URL') }}"></script>
        <script src="//yubinbango.github.io/yubinbango-core/yubinbango-core.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/gsap/1.20.3/TweenMax.min.js"></script>
        <script>
            function scrollTop(elem,duration) {
            let target = document.getElementById(elem);
                target.addEventListener('click', function() {
                    let currentY = window.pageYOffset;
                    let step = duration/currentY > 1 ? 10 : 10;
                    let timeStep = duration/currentY * step;
                    let intervalID = setInterval(scrollUp, timeStep);

                    function scrollUp(){
                        currentY = window.pageYOffset;
                        if(currentY === 0) {
                            clearInterval(intervalID);
                        } else {
                            scrollBy( 0, -step );
                        }
                    }
                });
            }
        </script>
        <script src="{{ asset('js/vue.bundle.js') }}"></script>
        <script>
            axios.post('/webhook').then((r) => {
                console.log(r);
            }).catch((e) => {
                console.log(e);
            })
        </script>
    </div>
</body>
</html>
