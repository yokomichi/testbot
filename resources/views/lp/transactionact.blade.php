@extends('lp.layouts.master')

@section('title')
次亜塩素酸水の Guard Water  (ガードウォーター)　本店 - 特定商取引法に基づく表記
@endsection

@section('breadcrumb')
    <div class="breadcrumb__wrapper">
        <ul class="breadcrumb__list">
            <li class="breadcrumb-home"><a href="/">TOP</a></li>
            <li>特定商取引法に基づく表記</li>
        </ul>
    </div><!--breadcrumb__wrapper end-->
@endsection

@section('content')
    <h2 class="headline">特定商取引法に基づく表記</h2>
    <div class="article__area">
        <dl class="dl__line dotted">
            <div>
                <dt>販売業者</dt>
                <dd>株式会社グランブルー</dd>
            </div>
            <div>
                <dt>運営責任者</dt>
                <dd>竹村 圭</dd>
            </div>
            <div>
                <dt>住所</dt>
                <dd>〒261-7133 千葉県千葉市美浜区中瀬2-6　WBGマリブウエスト33F</dd>
            </div>
            <div>
                <dt>電話番号</dt>
                <dd>043-216-3776（代）　※納品後フリーダイヤルになる<br/>（平日10:00～17:00 土・日・祝日／長期休暇は除く）</dd>
            </div>
            <div>
                <dt>FAX番号</dt>
                <dd>043-216-3776</dd>
            </div>
            <div>
                <dt>メールアドレス</dt>
                <dd><a href="mailto:jiae@momentum-arc.jp">jiae@momentum-arc.jp</a></dd>
            </div>
            <div>
                <dt>URL</dt>
                <dd><a href="https://jiae.jp">https://jiae.jp</a></dd>
            </div>
            <div>
                <dt>商品の価格</dt>
                <dd>詳しくはこちら　※リンクをつけるの忘れない</dd>
            </div>
            <div>
                <dt>その他手数料</dt>
                <dd>詳しくはこちら　　※リンクをつけるの忘れない</dd>
            </div>
            <div>
                <dt>代金支払方法</dt>
                <dd>クレジットカード、代金引換決済、NP後払い決済、公費・校費払い</dd>
            </div>
            <div>
                <dt>支払い時期</dt>
                <dd>
                    クレジットカード：各カード会社引き落とし日<Br>
                    代金引換：商品お届け時（現金のみ）<Br>
                    NP後払い決済：振込用紙発効日から14日以内 引渡し時期 在庫のある商品につきましては、<Br>
                    ご注文日から2営業日以内で発送 返品・交換 返品・交換が可能な商品のみ、商品到着後7日以内はお受けいたします。
                </dd>
            </div>
            <div>
                <dt>返品・交換</dt>
                <dd>返品・交換がご希望の場合は、メールにてその旨ご連絡ください。<br/>詳しくはこちら　　※リンクをつけるの忘れない</dd>
            </div>
        </dl>
    </div>
@endsection
