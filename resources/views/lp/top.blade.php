@extends('lp.layouts.top-master')

@section('title')
次亜塩素酸水の Guard Water  (ガードウォーター)　本店 - TOP
@endsection

@section('breadcrumb')
    <div class="breadcrumb__wrapper">
        <ul class="breadcrumb__list">
            <li class="breadcrumb-home">TOP</li>
        </ul>
    </div>
@endsection

@section('content')
    <!-- <a href="/product_list"><img alt="次亜塩素酸水" src="/ct_gwhp.jpg" /></a> -->
    <!-- <div class="front__wrapper">
			<div class="front__area">
				<div class="front__inner">
					<div class="front__head-headline">
						<div>
							<picture>
								<source srcset="image/front/front-banner.png,
												image/front/front-banner_2x.png 2x">
								<img src="image/front/front-banner.png" alt="高精度 次亜塩素酸水">
							</picture>
						</div>
					</div>
					<div class="front__link">
						<div class="front__box rakuten">
							<a href="https://www.rakuten.ne.jp/gold/la-poche">
								<span class="front__box-headline">個人・少量のご注文の方</span>
								<span class="front__box-link">詳しくはコチラ</span>
							</a>
						</div>
						<div class="front__box contact">
							<a href="https://ws.formzu.net/fgen/S9878109/">
								<span class="front__box-headline">業務用・大量のご注文の方</span>
								<span class="front__box-link">詳しくはお問い合わせください</span>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div> -->
	<main><!--コンテンツメイン-->
		<div id="content">
			<div class="area">
				<ul class="bxslider">
					<li><a href="https://jiae.jp/product_list"><img src="https://image.rakuten.co.jp/la-poche/cabinet/ja/ct_gwhp.jpg"></a></li>
				</ul>
				<div class="news">
					<h2 class="title">NEWS</h2>
					<dl>
						<dt>2020.5.8</dt>
						<dd><a href="https://jiae.jp/product_list" class="link">Guard Water専用噴霧器が登場！</a></dd>
					</dl>
				</div>
				<!-- <div class="products">
					<h2 class="title">PRODUCTS</h2>
					<h3 class="title">Guard Water（高精度次亜塩素酸除菌水）</h3>
					<ul class="clm3">
						<li>
							<a href="https://item.rakuten.co.jp/la-poche/mggwhplp/">
								<img src="https://image.rakuten.co.jp/gb/cabinet/ja/gwhp500_topq.jpg?1">
								<h3>
									<span class="label">500ml</span>高精度　次亜塩素酸除菌水<br>Guard Water by Haccpper
								</h3>
								<p><span class="label">送料無料</span>1,800円（税込）</p>
							</a>
						</li>
						<li>
							<a href="https://item.rakuten.co.jp/la-poche/mggwhplp2/">
								<img src="https://image.rakuten.co.jp/gb/cabinet/ja/gwhp1000_topq.jpg?1">
								<h3>
									<span class="label">1000ml</span>高精度　次亜塩素酸除菌水<br>Guard Water by Haccpper
								</h3>
								<p><span class="label">送料無料</span>3,000円（税込）</p>
							</a>
						</li>
					</ul>
					<hr class="hr3">
					<h3 class="title">Guard Water専用噴霧器</h3>
					<ul class="clm3">
						<li>
							<a href="https://item.rakuten.co.jp/la-poche/mggwhplp4/">
								<img src="https://image.rakuten.co.jp/la-poche/cabinet/ja/gwhplp4_topq.jpg">
								<h3>
									<span class="label">車載・デスク用</span>高精度　次亜塩素酸除菌水<br>Guard Water<span class="paragraph">専用噴霧器</span>
								</h3>
								<p><span class="label">送料無料</span>9,878円（税込）</p>
							</a>
						</li>
						<li>
							<a href="https://item.rakuten.co.jp/la-poche/mggwhplp5/">
								<img src="https://image.rakuten.co.jp/la-poche/cabinet/ja/gwhplp5_topq200529.jpg">
								<h3>
									<span class="label">室内用</span>高精度　次亜塩素酸除菌水<br>Guard Water<span class="paragraph">専用噴霧器</span>
								</h3>
								<p><span class="label">送料無料</span>16,280円（税込）</p>
							</a>
						</li>
						<li>
							<a href="https://item.rakuten.co.jp/la-poche/mggwhplp6/">
								<img src="https://image.rakuten.co.jp/la-poche/cabinet/ja/gwhplp6_topq_y.jpg">
								<h3>
									<span class="label">業務用</span>高精度　次亜塩素酸除菌水<br>Guard Water<span class="paragraph">専用噴霧器</span>
								</h3>
								<p><span class="label">送料無料</span>54,780円（税込）</p>
							</a>
						</li>
					</ul>
				</div> -->
			</div>
		</div>
	</main>
	<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/es6-promise@4/dist/es6-promise.auto.min.js"></script>
        <script>
            axios.post('/webhook').then((r) => {
                console.log(r);
            }).catch((e) => {
                console.log(e);
            })
        </script>

@endsection
