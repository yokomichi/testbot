<header class="">
    <div class="header__wrapper">
        <div class="header__logo">
            @auth
                <!-- <a href="/mypage"></a> -->
                <a href="/"></a>
            @endauth
            @guest
                <a href="/"></a>
            @endguest
        </div>
        <!-- <div class="header__area">
            <div class="header__nav">
                <h1 class="header__headline">@yield('title')</h1>
                <ul class="menu__option">
                    <li class="mypage"><a href="/mypage">マイページ</a></li>
                    <li class="cart"><a href="/cart"><span class="shoppingcart__number" id="shoppingcart__number">0</span>カート</a></li>
                </ul>
            </div>

            <div class="menu__wrapper" onclick="changeMenu()">
                <div class="menu__area">
                    <div class="menu-trigger">
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                    <span class="menu__headline">MENU</span>
                </div>
            </div>--><!--menu__wrapper end-->
        <!-- </div>  -->
    </div>
</header>
<script>
window.onload = function() {
    axios.post('/axios/getitemcount', {session: '<?php echo \Session::get('_token') ?>' }).then(function (res) {
        var target = document.getElementById('shoppingcart__number');
        if (target) {
            target.innerHTML = res.data;
        }
    });
}
var toggle = false;
function changeMenu () {
    toggle = !toggle;

    if (toggle) {
    document.getElementsByClassName('menu__wrapper')[0].classList.add("active");
    document.body.classList.add("no-scroll");
    document.getElementsByClassName('nav__wrapper')[0].style.display = 'block';
    } else {
        document.getElementsByClassName('menu__wrapper')[0].classList.remove("active");
        document.body.classList.remove("no-scroll");
        document.getElementsByClassName('nav__wrapper')[0].style.display = 'none';
    }
}

</script>
