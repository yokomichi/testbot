<!DOCTYPE html>
<html lang="ja">
    <head>
        <title>@yield('title')</title>
        @include('lp.layouts.head')
    </head>
    <body>
        <!-- Google Tag Manager (noscript) -->
        <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MJGFJN3"
        height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        <!-- End Google Tag Manager (noscript) -->
        <div class="wrapper">
            @include('lp.layouts.header')
            <main>
                @yield('breadcrumb')
                <div class="main__wrapper">
                    <!-- <div class="content__wrapper"> -->
                        
                        <div class="content__area">
                            @yield('content')
                        </div>
                    <!-- </div> -->
                    <!--content__wrapper end-->
                </div><!--main__wrapper end-->
                <span class="page__top-nav"><a id="page_top"></a></span>
            </main>
            @include('lp.layouts.footer')
        </div>
    </body>
    <div class="back-image">
        <img src="/back-image.jpg" />
    </div>
    <script src="//yubinbango.github.io/yubinbango-core/yubinbango-core.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/gsap/1.20.3/TweenMax.min.js"></script>
    <script src="{{ asset('js/vue.bundle.js') }}"></script>
    <script>
        scrollTop('page_top', 500);
        function scrollTop(elem,duration) {
        let target = document.getElementById(elem);
            target.addEventListener('click', function() {
                let currentY = window.pageYOffset;
                let step = duration/currentY > 1 ? 10 : 100;
                let timeStep = duration/currentY * step;
                let intervalID = setInterval(scrollUp, timeStep);

                function scrollUp(){
                    currentY = window.pageYOffset;
                    if(currentY === 0) {
                        clearInterval(intervalID);
                    } else {
                        scrollBy( 0, -step );
                    }
                }
            });
        }
    </script>
</html>
