<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-EVMGNGR325"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-EVMGNGR325');
</script>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="format-detection" content="telephone=no">
<meta name="description" content="次亜塩素酸水販売のDEMO SPIDER（グランブルー）" />
<meta name="keywords" content="DEMO SPIDER,グランブルー,水,次亜塩素酸水">
<link rel="stylesheet" href="{{ asset('css/pc.bundle.css') }}" media="print,screen and (min-width: 1024px)">
<link rel="stylesheet" href="{{ asset('css/sp.bundle.css') }}" media="screen and (max-width: 1023px)">
<link href="{{ asset('css/print.css') }}" media="print" rel="stylesheet" type="text/css">
@if (App::environment('local', 'staging'))
<meta name="robots" content="noindex" />
@endif
<meta name="csrf-token" content="{{ csrf_token() }}">
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-MJGFJN3');</script>
<!-- End Google Tag Manager -->