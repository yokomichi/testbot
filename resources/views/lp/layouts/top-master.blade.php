<!DOCTYPE html>
<html lang="ja">
    <head>
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-EVMGNGR325"></script>
        <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'G-EVMGNGR325');
        </script>
        <title>高精度　次亜塩素酸除菌水のGuard Water ガードウォーター | トップページ</title>

        <meta name="description" content="除菌・消臭効果のある次亜塩素酸水を使用した、安心・安全の除菌・消臭水の【Guard Water(ガードウォーター)】。お子様、ご高齢の方や、ペットのいるご家庭にもご利用いただけます" />
        <meta name="keyword" content="次亜塩素酸,消臭,除菌,ガードウォーター" />

        <link rel="stylesheet" href="{{ asset('/css/pc.bundle.css') }}" media="print,screen and (min-width: 1024px)">
        <link rel="stylesheet" href="{{ asset('/css/sp.bundle.css') }}" media="screen and (max-width: 1023px)">

        <link rel="stylesheet" href="{{ asset('/top/css/reset.css') }}" />
        <link rel="stylesheet" href="{{ asset('/top/css/style.css') }}" />

        <script src="https://kit.fontawesome.com/47683631a7.js" crossorigin="anonymous"></script>

        <meta name="viewport" content="width=device-width; initial-scale=1.0" />


        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta name="google-site-verification" content="hsa_9fq6iKTIvhw52iJHhyt0OiehGOa9hvlvGQ3jNmc" />
        @if (App::environment('local', 'staging'))
        <meta name="robots" content="noindex" />
        @endif
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <!-- Google Tag Manager -->
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-MJGFJN3');</script>
        <!-- End Google Tag Manager -->
    </head>
    <body id="page-top" class="front">
        <!-- Google Tag Manager (noscript) -->
        <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MJGFJN3"
        height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        <!-- End Google Tag Manager (noscript) -->
        <header><!--共通ヘッダ-->
            <div id="header">
                <div id="header_top">
                    <!-- <div id="info" class="box1">
                        <h2>ゴールデンウィーク期間中の営業・配送につきまして</h2>
                        <p><a href="information2.html" target="_blank" class="link">詳しくはこちら</a></p>
                    </div> -->
                    <h1>優しい除菌・消臭水【次亜塩素酸水】GuardWater(ガードウォーター)</h1>
                    <ul class="header_menu">
                        <!-- <li><a href="/guide" class="link">お買い物ガイド</a></li> -->
                        <li><a href="/mypage" class="link">マイページ</a></li>
                    </ul>
                </div>
                <div class="header_lgray">
                    <div class="area">
                        <h2 class="logo"><a href="/">次亜塩素酸水の<img src="img/common/logo.png"></a></h2>
                        <div class="box4">
                            <h3>業務用大量ご注文の方</h3>
                            <p><a href="https://ws.formzu.net/fgen/S9878109/" target="_blank" class="btn1">詳しくはこちら</a></p>
                        </div>
                    </div>
                    <div id="nav">
                        <ul>
                            <li><a href="/"><img src="img/common/icon_gray_home.png" />TOP</a></li>
                            <!-- <li><a href="/about"><img src="img/common/icon_gray_memo.png" />ガードウォーターとは</a></li> -->
                            <li><a href="/product_list"><img src="img/common/icon_gray_drop.png" />商品一覧</a></li>
                            <!-- <li><a href="/qa"><img src="img/common/icon_gray_bubble.png" />よくある質問</a></li> -->
                            <li><a href="/cart"><img src="img/common/icon_gray_cart.png" />お買い物かご</a></li>
                        </ul>
                    </div>
                    <div id="nav_sp">
                        <div class="box">
                            <a class="button_cart" href="https://order.step.rakuten.co.jp/rms/mall/basket/vc?shop_bid=268511&__event=ES01_003_001"><img src="img/common/sp_cart.png" width="40%"></a>
                        </div>
                        <div class="box">
                            <a class="button" href="#sp_menu"><img src="img/common/sp_menu.png" width="40%"></a>
                        </div>
                        <div id="sp_menu" class="overlay">
                            <div class="popup">
                                <a class="close" href="#nav_sp">×</a>
                                <div class="content">
                                    <ul>
                                        <li><a href="https://ws.formzu.net/fgen/S9878109/">業務用大量ご注文の方</a></li>
                                    </ul>
                                    <ul>
                                        <li><a href="/mypage"><img src="img/common/icon_gray_home.png" />マイページ</a></li>
                                    </ul>
                                    <ul>
                                        <li><a href="/"><img src="img/common/icon_gray_home.png" />TOP</a></li>
                                        <li><a href="/about"><img src="img/common/icon_gray_memo.png" />ガードウォーターとは</a></li>
                                        <li><a href="/product_list"><img src="img/common/icon_gray_drop.png" />商品一覧</a></li>
                                        <li><a href="/qa"><img src="img/common/icon_gray_bubble.png" />よくある質問</a></li>
                                        <li><a href="/cart"><img src="img/common/icon_gray_cart.png" />お買い物かご</a></li>
                                    </ul>
                                    <ul>
                                        <li><a href="aboutus.html#aboutus2">お買い物ガイド</a></li>
                                        <li><a href="aboutus.html#aboutus1">会社概要</a></li>
                                        <li><a href="privacy.html">個人情報ポリシー</a></li>
                                        <li><a href="/transactionactact">特定商取引法</a></li>
                                        <li><a href="/contact">お問合せ</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div><!--sp_menu-->
                    </div><!--nav_sp--->
                </div><!--header_lgray-->
                <!-- <p class="ib_banner"><a href="/news/200601.html" target="_blank" class="btn1">2020年5月29日のNITE・経産省の発表と<span class="paragraph">ミスリード報道について</span></a></p> -->
            </div>
        </header><!--共通ヘッダ-->
        @yield('content')
        <span class="page__top-nav"><a id="page_top"></a></span>
        <footer><!--共通フッタ-->
            <div id="footer">
                <!-- <div class="area">
                    <h2 class="title">SHOPPING GUIDE</h2>
                    <div class="guide">
                        <ul class="clm2">
                            <li>
                                <div>
                                    <h3>次亜塩素酸水のGuard Water</h3>
                                    <p>営業時間 10:00－17:30（平日）<br>※土曜・日曜・祝日は定休日</p>
                                    <p><a href="https://ask.step.rakuten.co.jp/rms/mall/pa/ask/vc?__event=PA02_000_001&ask_text=%C5%B9%C4%B9%A4%CB%BC%C1%CC%E4%A4%B9%A4%EB&shopurl=la-poche&shop_bid=268511&shop_name=la+poche&myshop=0" class="btn1">お問い合わせはこちら</a></p>
                                </div>
                                <div>
                                    <h3>送料について</h3>
                                    <p><strong>宅配便　　全国一律450円（税込）</strong></p>
                                    <p>※お買上げ金額が3,980円（税込）以上で送料無料<br>※離島・一部地域も追加送料はかかりません<br>※宅配便は佐川急便となります</p>
                                </div>
                                <div>
                                    <h3>お支払い方法</h3>
                                    <p>以下の決済方法がお選びいただけます。</p>
                                    <ul class="list">
                                        <li>クレジットカード</li>
                                        <li>銀行振込</li>
                                        <li>Apple Pay</li>
                                        <li>後払い決済</li>
                                        <li>代金引換</li>
                                        <li>コンビニ前払い</li>
                                    </ul>
                                    <p class="more"><a href="/aboutus.html#aboutus2" class="link">詳しくはこちら</a></p>
                                </div>
                            </li>
                            <li>
                                <div>
                                    <h3>お買い物ガイド</h3>
                                    <ul class="clm2">
                                        <li><a href="/guide#guide1" class="btn2">ご注文の流れ</a></li>
                                        <li><a href="/guide#guide2" class="btn2">お支払い方法</a></li>
                                        <li><a href="/guide#guide3" class="btn2">配送方法</a></li>
                                        <li><a href="/guide#guide4" class="btn2">交換・返品</a></li>
                                        <li><a href="/guide#guide6" class="btn2">よくある質問</a></li>
                                    </ul>
                                </div>
                                <div>
                                    <h3>営業日カレンダー</h3>
                                    <div id="cal0" class="cal_wrapper">Calendar Loading</div>
                                    <div id="cal1" class="cal_wrapper">Calendar Loading</div>
                                    <script src="{{ asset('top/js/cal.js') }}"></script>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div> -->
                <div class="footer_link">
                    <div class="area">
                        <p>
                            <a href="/" class="link">TOP</a>
                            <a href="/company" class="link">会社概要</a>
                            <a href="/terms" class="link">利用規約</a>
                            <a href="/policy" class="link">個人情報ポリシー</a>
                            <a href="/transactionact" class="link">特定商取引法</a>
                            <!-- <a href="/guide" class="link">お買い物ガイド</a>
                            <a href="/qa" class="link">よくある質問</a> -->
                            <a href="/contact" class="link">お問合せ</a></p>
                    </div>
                    <p class="copyright">Copyright c DEMO SPIDER Co.,Ltd 2020 All rights Reserved.</p>
                </div>
            </div>
        </footer><!--共通フッタ-->
        <script>
            scrollTop('page_top', 500);
            function scrollTop(elem,duration) {
            let target = document.getElementById(elem);
                target.addEventListener('click', function() {
                    let currentY = window.pageYOffset;
                    let step = duration/currentY > 1 ? 10 : 100;
                    let timeStep = duration/currentY * step;
                    let intervalID = setInterval(scrollUp, timeStep);

                    function scrollUp(){
                        currentY = window.pageYOffset;
                        if(currentY === 0) {
                            clearInterval(intervalID);
                        } else {
                            scrollBy( 0, -step );
                        }
                    }
                });
            }
        </script>
    </body>
</html>
