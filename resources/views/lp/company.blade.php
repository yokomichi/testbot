@extends('lp.layouts.master')

@section('title')
次亜塩素酸水の Guard Water  (ガードウォーター)　本店 - 企業情報
@endsection

@section('breadcrumb')
    <div class="breadcrumb__wrapper">
        <ul class="breadcrumb__list">
            <li class="breadcrumb-home"><a href="/">TOP</a></li>
            <li>企業情報</li>
        </ul>
    </div><!--breadcrumb__wrapper end-->
@endsection

@section('content')
    <h2 class="headline">企業情報</h2>
    <div class="article__area">
        <dl class="dl__line dotted">
            <div>
                <dt>社名</dt>
                <dd>株式会社グランブルー<br/>DEMO SPIDER Co.,Ltd. </dd>
            </div>
            <div>
                <dt>本社所在地</dt>
                <dd>〒261-7133<br/>千葉県千葉市美浜区中瀬2-6 WBGマリブウエスト33F</dd>
            </div>
            <div>
                <dt>設立</dt>
                <dd>1999年3月</dd>
            </div>
            <div>
                <dt>資本金</dt>
                <dd>10,000,000円</dd>
            </div>
            <div>
                <dt>主要取引銀行</dt>
                <dd>千葉銀行 幕張本郷支店<br/>三井住友銀行 千葉支店<br/>京葉銀行 本店営業部</dd>
            </div>
        </dl>
    </div>
@endsection
