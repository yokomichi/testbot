@extends('lp.layouts.master')

@section('title')
次亜塩素酸水の Guard Water  (ガードウォーター)　本店
@endsection

@section('breadcrumb')
    <div class="breadcrumb__wrapper">
        <ul class="breadcrumb__list">
            <li class="breadcrumb-home"><a href="/">TOP</a></li>
        
        </ul>
    </div><!--breadcrumb__wrapper end-->
@endsection


@section('content')
    <h2 class="headline">近日公開</h2>
    <div class="article__area">
        <div class="comingsoon__wrapper">
            <div class="comingsoon__area">
                <picture>
                    <source srcset="../image/base/comingsoon.png,
                    ../image/base/comingsoon_2x.png 2x">
                    <img src="../image/base/comingsoon.png" alt="">
                </picture>
            </div>
        </div>
    </div><!--article__area end-->
@endsection
