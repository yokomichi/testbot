@extends('lp.layouts.master')

@section('title')
次亜塩素酸水の Guard Water  (ガードウォーター)　本店 - お知らせ
@endsection

@section('breadcrumb')
    <div class="breadcrumb__wrapper">
        <ul class="breadcrumb__list">
            <li class="breadcrumb-home"><a href="/">TOP</a></li>
            <li>お知らせ</li>
        </ul>
    </div><!--breadcrumb__wrapper end-->
@endsection

@section('content')
    <h2 class="headline">お知らせページを入れて下さい</h2>
@endsection
