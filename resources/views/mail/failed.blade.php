{{ isset($member_info['company_name']) ? $member_info['company_name'] : '' }}
{{ $member_info['first_name'] }} {{ $member_info['last_name'] }}様

ご注文いただいております以下の購入で、ご登録
のクレジットカードが、決済不能との返答がクレジット
カードの発行会社よりございました。

誠におそれいりますが、クレジットカードのご利用に関して
ご確認のうえ、ご利用可能なクレジットカード情報の
再登録、もしくは別の決済方法への変更をお願いいたします。


━━━━━━ 【定期購入情報】 ━━━━━━

■ 注文日時 : {{ date('Y-m-d (H:i:s)') }}
■ 購入ID : {{ $order['order_id'] }}

<?php $total = 0; ?>
@foreach ($order_detail as $v)

【※ 商品名 : {{ $v['item_main_name'] }} 】
【※ 商品コード : {{ $v['item_id'] }} 】
【※ 個数 : {{ number_format($v['quantity']) }} 】
【※ 小計(税込) : {{ number_format($v['selling_price']) }}】
【※ 配送方法 : クロネコヤマト 】
【※ 送料 : {{ number_format($v['postage']) }}円】
<?php $total += $v['detail_total']; ?>

@endforeach
【-----------------------------------------------------------】
【商品合計(税込) : {{number_format($total) }}円】
【送料計     : {{number_format($order['postage_total']) }}円】
【代引手数料   : {{number_format($order['shipment_price']) }}円】
【------------------------------------------】
【請求総額 : {{number_format($order['total']) }}円】


━━━━━━━━━━━━━━━━━━━━━━━━━

  クレジットカード情報の更新、決済方法の変更方法

━━━━━━━━━━━━━━━━━━━━━━━━━

会員登録されている方はログイン後、下記のURLからクレジットカードの登録情報を変更して下さい。
{{ env('FRONT_URL').'/mypage/payment' }}

