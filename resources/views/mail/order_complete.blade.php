
@if(!empty($member_info))
{{ isset($member_info['company_name']) ? $member_info['company_name'] : '' }}
{{ $member_info['first_name'] }} {{ $member_info['last_name'] }}様
@else
{{ isset($order['company_name']) ? $order['company_name'] : '' }}
{{ $order['first_name'] }} {{ $order['last_name'] }} 様
@endif
この度は「DEMO SPIDER」を
ご利用頂きありがとうございます。

また、ご注文頂きまして、誠にありがとうございます。
ご注文について、下記の通り承りました。
別途、当店から再度、ご注文確認のメールをお送り致します。



■■■■■ 注文情報 ■■■■■

{{ date('Y-m-d (H:i:s)') }}

@foreach($product as $v)
※ 商品名 : {{ $v['item_main_name'] }}
※ 商品コード : {{ $v['item_id'] }}
※ 小計 : {{ number_format(($v['selling_price'] + $tax_total = $v['tax_division'] == 1 ? 0 : floor($v['unit_price'] * $config['tax_rate_division'][$v['tax_rate_division']])) * $v['item_count']) }}円({{ $v['tax_division'] == '2' ? '税抜' : '税込' }})
※ 配送方法 : クロネコヤマト
@endforeach
-----------------------------------------------------------
商品合計(税込) :  {{ number_format($add_register['selling_price_total']) }}円
送料     : {{ number_format($add_register['postage_total']) }}円
@if($add_register['payment'] == 2)
代引手数料   : {{ number_format($add_register['shipment_price']) }}円
@endif
-----------------------------------------------------------
請求総額 : {{ number_format($add_register['total']) }}円

■■■■■ 注文者情報 ■■■■■

@if(!empty($member_info))
※ 名 前 : {{ isset($member_info['company_name']) ? $member_info['company_name'] : '' }} {{ $member_info['first_name'] }} {{ $member_info['last_name'] }} 様
※ 電話番号 : {{ $member_info['phone'] }}
※ E-MAIL : {{ $member_info['mail'] }}
※ 住 所 : (〒{{ $member_info['post'] }}) {{ $config['pref'][$member_info['pref']] }} {{ $member_info['city'] }} {{ $member_info['addr'] }} {{ isset($member_info['buliding']) ? $member_info['buliding'] : ''}}
@else
※ 名 前 : {{ isset($order['company_name']) ? $order['company_name'] : '' }} {{ $order['first_name'] }} {{ $order['last_name'] }} 様
※ 電話番号 : {{ $order['phone'] }}
※ E-MAIL : {{ $order['mail'] }}
※ 住 所 : (〒{{ $order['post'] }}) {{ $config['pref'][$order['pref']] }} {{ $order['city'] }} {{ $order['addr'] }} {{ isset($order['buliding']) ? $order['buliding'] : ''}}
@endif

■■■■■ 商品のお届け先 ■■■■■

※ 名 前 : {{ isset($order['company_name']) ? $order['company_name'] : '' }} {{ $order['first_name'] }} {{ $order['last_name'] }} 様
※ 電話番号 : {{ $order['phone'] }}
※ 都道府県 : {{ $config['pref'][$order['pref']] }}
※ 住 所 : (〒{{ $order['post'] }}) {{ $config['pref'][$order['pref']] }} {{ $order['city'] }} {{ $order['addr'] }} {{ isset($order['buliding']) ? $order['buliding'] : ''}}
※ 配送希望日 : {{ $add_register['shipment_hope_date'] }}

■■■■■ 決済情報 ■■■■■

{{ $config['payment'][$add_register['payment']] }}

