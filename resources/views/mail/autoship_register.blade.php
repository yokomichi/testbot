{{ isset($member_info['company_name']) ? $member_info['company_name'] : '' }}
{{ $member_info['first_name'] }} {{ $member_info['last_name'] }}様

この度は当店をご利用頂きまして誠にありがとうございます。


今回のご注文は以下の通り承りましたので内容をご確認下さい。

———————————————————————————————

[ご注文者様]
{{ isset($member_info['company_name']) ? $member_info['company_name'] : '' }}
{{ $member_info['first_name'] }} {{ $member_info['last_name'] }}様

[メールアドレス] {{ $member_info['mail'] }}

———————————————————————————————
【注文番号】{{ $shipment_array['order_id'] }}

【決済方法】{{ $config['payment'][$shipment_array['payment']] }}

【お届け先の都道府県】
(〒{{ $shipment_array['post'] }}) {{ $config['pref'][$shipment_array['pref']] }} {{ $shipment_array['city'] }} {{ $shipment_array['addr'] }} {{ isset($shipment_array['buliding']) ? $shipment_array['buliding'] : ''}}
———————————————————————————————

{{ date('Y-m-d (H:i:s)') }}


@foreach($product as $k => $v)
※ 商品名 : {{ $v['item_main_name'] }}
※ 商品コード : {{ $v['item_id'] }}
※ 小計 : {{ number_format(($v['selling_price'] + $tax_total = $v['tax_division'] == 1 ? 0 : floor($v['unit_price'] * $config['tax_rate_division'][$v['tax_rate_division']])) * $v['quantity']) }}円({{ $v['tax_division'] == '2' ? '税抜' : '税込' }})
※ 配送方法 : クロネコヤマト
@endforeach

【-----------------------------------------------------------】
消費税: {{ number_format($tax_total) }}円
商品合計(税込) :  {{ number_format($shipment_array['selling_price_total']) }}円
送料     : {{ number_format($shipment_array['postage_total']) }}円
@if($shipment_array['payment'] == 2)
代引手数料   : {{ number_format($shipment_array['shipment_price']) }}円
@endif
-----------------------------------------------------------
請求総額 : {{ number_format($shipment_array['total']) }}円

———————————————————————————————
【受注コメント】
———————————————————————————————

【商品のお届けについて】
お荷物の[発送日]・[配送伝票番号]につきましては、
こちらのメールの後に送信されます【発送完了メール】にて
ご確認いただけますようお願い致します。　
※到着日のご指定のないお客様は、最速日で発送準備をしております。

【ネコポス便(ポスト投函便)対象商品をご注文のお客様へ】
・「到着日時のご指定」ができません。
・「送付先変更」ができません。
・「荷物はポストへお届け」となります。
・紛失や盗難などがありましても「配送補償」はございません。


発送作業が完了いたしましたら、【発送完了メール】にて
発送日やお荷物の伝票番号について、ご案内させていただきます。


