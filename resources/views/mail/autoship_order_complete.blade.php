
{{ isset($member_info['company_name']) ? $member_info['company_name'] : '' }}
{{ $member_info['first_name'] }} {{ $member_info['last_name'] }}様

この度は「DEMO SPIDER」を
ご利用頂きありがとうございます。

また、定期購入のご注文を頂きまして、誠にありがとうございます。
ご注文について、下記の通り承りました。


━━━━━━ 【定期購入情報】 ━━━━━━

■ 注文日時：{{ date('Y-m-d (H:i:s)') }}
■ 定期購入ID :{{ $autoship_id }}
■ 定期商品名 :{{ $autoship_item['autoship_item_name'] }}
<?php $total = 0; ?>
@foreach ($autoship_item_detail as $v)

【※ 商品名 : {{ $v['post_title'] }} 】
【※ 商品コード : {{ $v['item_id'] }} 】
【※ 個数 : {{ number_format($v['quantity']) }} 】
【※ 小計(税込) : {{ number_format($v['selling_price']) }}】
【※ 配送方法 : クロネコヤマト 】
【※ 送料 : {{ number_format($v['postage']) }}円】
<?php $total += $v['detail_total']; ?>

@endforeach
【-----------------------------------------------------------】
【商品合計(税込) : {{number_format($total) }}円】
【送料計     : {{number_format($autoship_item_main['postage_total']) }}円】
【代引手数料   : {{number_format($add_register['shipment_price']) }}円】
【------------------------------------------】
【請求総額 : {{number_format($autoship_item_main['total']) }}円】



━━━━━━  【ご注文情報】  ━━━━━━

【注文番号】

■お届け回数：1回目
■お届け予定日：{{date("Y-m-d", strtotime("5 day"))}}

 【注文者情報】
■ 名 前 :
{{ isset($member_info['company_name']) ? $member_info['company_name'] : '' }}
{{ $member_info['first_name'] }} {{ $member_info['last_name'] }}様
■ 電話番号 : {{ $member_info['phone'] }}
■ E-MAIL : {{ $member_info['mail'] }}

■ 住 所 : (〒{{ $member_info['post'] }}) {{ $config['pref'][$member_info['pref']] }} {{ $member_info['city'] }} {{ $member_info['addr'] }} {{ isset($member_info['buliding']) ? $member_info['buliding'] : ''}}

━━━━━━【商品のお届け先】━━━━━

■ 名 前 :
{{ isset($send_info['company_name']) ? $send_info['company_name'] : '' }}
{{ $send_info['first_name'] }} {{ $send_info['last_name'] }}様
■ 電話番号 : {{ $send_info['phone'] }}
■ E-MAIL : {{ $send_info['mail'] }}

■ 住 所 : (〒{{ $send_info['post'] }}) {{ $config['pref'][$send_info['pref']] }} {{ $send_info['city'] }} {{ $send_info['addr'] }} {{ isset($send_info['buliding']) ? $send_info['buliding'] : ''}}

━━━━━━━━━━━━━━━━━━━━


━━━━━━━【決済情報】━━━━━━━

{{ $config['payment'][$add_register['payment']] }}

