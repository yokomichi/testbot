会員番号：{{ $member_id }}
{{ $first_name }} {{ $last_name }} 様

次回お届け内容の変更が完了いたしました。変更内容のご確認をお願いいたします。

{{ $first_name }} 様のご注文番号は {{ $order_id }}です。
当ご注文についてのお問合せはこちらのご注文番号が必要となりますので、製品の配送が完了するまでお控えください。

【配送先情報】
   {{ isset($order['company_name']) ? $order['company_name'] : '' }}
   {{ $order['first_name'] }} {{ $order['last_name'] }} 様
   〒{{ $order['post'] }} {{ $config['pref'][$order['pref']] }} {{ $order['city'] }} {{ $order['addr'] }} {{ isset($order['buliding']) ? $order['buliding'] : ''}}
   TEL：{{ $order['phone'] }}

================================================================================

【ご注文商品】
［注文商品一覧］
@foreach($register_detail_params as $v)

    商品番号:{{ $v['item_id'] }}
    商品名:{{ $v['item_main_name'] }}
    数量:{{ number_format($v['quantity']) }}
    価格:¥{{ number_format($v['selling_price']) }}
    配送予定日:{{ $v['shipment_hope_date'] }}
@endforeach


   決済金額合計：¥{{ number_format($order['total']) }}
   お支払方法：{{ $config['payment'][$order['payment']] }}

================================================================================

