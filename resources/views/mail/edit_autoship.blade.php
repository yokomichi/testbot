
{{ isset($member_info->company_name) ? $member_info->company_name : '' }}
{{ $member_info->first_name }} {{ $member_info->last_name }}様

この度は「DEMO SPIDER」を
ご利用頂きありがとうございます。

ご注文いただいております以下の定期購入で、
注文内容を変更いたしましたのでお知らせいたします。


━━━━━━ 【定期購入情報】 ━━━━━━

■ 注文日時：{{ date('Y-m-d (H:i:s)') }}
■ 定期購入ID :{{ $autoship['autoship_id'] }}

<?php $total = 0; ?>
@foreach ($autoship_detail as $v)

【※ 商品名 : {{ $v['item_main_name'] }} 】
【※ 商品コード : {{ $v['item_id'] }} 】
【※ 個数 : {{ number_format($v['quantity']) }} 】
【※ 小計(税込) : {{ number_format($v['selling_price']) }}】
【※ 配送方法 : クロネコヤマト 】
【※ 送料 : {{ number_format($v['postage']) }}円】
<?php $total += $v['detail_total']; ?>

@endforeach

━━━━━━  【ご注文情報】  ━━━━━━

【注文者情報】
■ 名 前 :
{{ isset($member_info->company_name) ? $member_info->company_name : '' }}
{{ $member_info->first_name }} {{ $member_info->last_name }}様
■ 電話番号 : {{ $member_info->phone }}
■ E-MAIL : {{ $member_info->mail }}

■ 住 所 : (〒{{ $member_info->post }}) {{ $config['pref'][$member_info->pref] }} {{ $member_info->city }} {{ $member_info->addr }} {{ isset($member_info->buliding) ? $member_info->buliding : ''}}



