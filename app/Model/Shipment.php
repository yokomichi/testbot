<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use SoftDeletes;
class Shipment extends Model
{
    protected $table        = 'shipment';
    protected $primaryKey   = 'shipment_id';
    protected $guarded      = ['shipment_id'];
    protected $casts = [
        'shipment_id' => 'string',
        'member_id' => 'string',
        'order_id' => 'string',
    ];
    public function getCreatedAtAttribute($date)
    {
        return date('Y-m-d', strtotime($date));
    }

    public function getUpdatedAtAttribute($date)
    {
        return date('Y-m-d', strtotime($date));
    }

}
