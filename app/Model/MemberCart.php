<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class MemberCart extends Model
{
    const UPDATED_AT = null;

    protected $table        = 'member_cart';
    protected $primaryKey   = 'id';
    protected $guarded      = ['id'];
    protected $casts = [
        'member_id' => 'string',
        'item_id' => 'string',
    ];
}
