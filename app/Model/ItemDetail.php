<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ItemDetail extends Model
{
    const UPDATED_AT = null;

    protected $table        = 'item_detail';
    protected $primaryKey   = 'id';
    protected $guarded      = ['item_id'];
    protected $casts = [
        'item_id' => 'string',
    ];

}
