<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class OrderDetail extends Model
{
    const UPDATED_AT = null;

    protected $table        = 'order_detail';
    protected $primaryKey   = 'id';
    protected $guarded      = ['order_id'];
    protected $casts = [
        'order_id' => 'string',
        'item_id' => 'string',
    ];

}
