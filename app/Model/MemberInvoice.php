<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class MemberInvoice extends Model
{
    const UPDATED_AT = null;

    protected $table        = 'member_invoice';
    protected $primaryKey   = 'member_id';
    protected $guarded      = ['member_id'];
    protected $casts = [
        'member_id' => 'string',
    ];
    public function getCreatedAtAttribute($date)
    {
        return date('Y-m-d', strtotime($date));
    }

    public function getUpdatedAtAttribute($date)
    {
        return date('Y-m-d', strtotime($date));
    }
}
