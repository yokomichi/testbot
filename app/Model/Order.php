<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use SoftDeletes;
class Order extends Model
{
    protected $table        = 'order';
    protected $primaryKey   = 'order_id';
    protected $guarded      = ['order_id'];
    protected $casts = [
        'order_id' => 'string',
        'member_id' => 'string',
        'autoship_id' => 'string',
    ];
    public function getCreatedAtAttribute($date)
    {
        return date('Y-m-d', strtotime($date));
    }

    public function getUpdatedAtAttribute($date)
    {
        return date('Y-m-d', strtotime($date));
    }

}
