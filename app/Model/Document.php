<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use SoftDeletes;
class Document extends Model
{
    protected $table        = 'document';
    protected $primaryKey   = 'id';
    protected $guarded      = ['id'];
    public function getCreatedAtAttribute($date)
    {
        return date('Y-m-d', strtotime($date));
    }

    public function getUpdatedAtAttribute($date)
    {
        return date('Y-m-d', strtotime($date));
    }
}
