<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Member extends Model
{
    protected $table        = 'member';
    protected $primaryKey   = 'member_id';
    protected $guarded      = ['member_id', 'org_member_id'];
    protected $casts = [
        'member_id' => 'string',
        'org_member_id' => 'string',
    ];
    public function getCreatedAtAttribute($date)
    {
        return date('Y-m-d', strtotime($date));
    }

    public function getUpdatedAtAttribute($date)
    {
        return date('Y-m-d', strtotime($date));
    }
}