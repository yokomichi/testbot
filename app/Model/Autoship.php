<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use SoftDeletes;

class Autoship extends Model
{
    protected $table        = 'autoship';
    protected $primaryKey   = 'autoship_id';
    protected $guarded      = ['autoship_id'];
    protected $casts = [
        'autoship_id' => 'string',
        'member_id' => 'string',
        'item_id' => 'string',
    ];
    public function getCreatedAtAttribute($date)
    {
        return date('Y-m-d', strtotime($date));
    }

    public function getUpdatedAtAttribute($date)
    {
        return date('Y-m-d', strtotime($date));
    }

}
