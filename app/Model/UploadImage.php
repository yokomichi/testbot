<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class UploadImage extends Model
{
    protected $table        = 'upload_image';
    protected $primaryKey   = 'id';
    protected $guarded      = ['id'];

    public function getFileUrlAttribute($path)
    {
        return url('/').$path;
    }

    public function getCreatedAtAttribute($date)
    {
        return date('Y-m-d', strtotime($date));
    }

    public function getUpdatedAtAttribute($date)
    {
        return date('Y-m-d', strtotime($date));
    }
}
