<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class AutoshipItemInterval extends Model
{
    protected $table        = 'autoship_item_interval';
    protected $primaryKey   = 'id';
    protected $guarded      = ['autoship_item_id'];
    protected $casts = [
        'autoship_item_id' => 'string',
    ];

}
