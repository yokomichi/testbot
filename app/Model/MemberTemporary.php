<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class MemberTemporary extends Model
{
    protected $table        = 'member_temporary';
    protected $primaryKey   = 'id';
    protected $guarded      = ['member_id'];
    protected $casts = [
        'member_id' => 'string',
    ];

}
