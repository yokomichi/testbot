<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use SoftDeletes;
class MasterConfig extends Model
{
    protected $table        = 'master_config';
    protected $primaryKey   = 'config_id';
    protected $guarded      = ['config_id'];
    public function getCreatedAtAttribute($date)
    {
        return date('Y-m-d', strtotime($date));
    }

    public function getUpdatedAtAttribute($date)
    {
        return date('Y-m-d', strtotime($date));
    }
}
