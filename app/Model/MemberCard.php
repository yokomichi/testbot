<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class MemberCard extends Model
{
    protected $table        = 'member_card';
    protected $casts = [
        'member_id' => 'string',
    ];
    protected $fillable = ['member_id', 'credit_number', 'credit_name', 'credit_expire_year', 'credit_expire_month', 'credit_token'];
    public $timestamps = false;
}