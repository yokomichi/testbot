<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class AutoshipItem extends Model
{
    protected $table        = 'autoship_item';
    protected $primaryKey   = 'id';
    protected $guarded      = ['autoship_item_id'];
    protected $casts = [
        'autoship_item_id' => 'string',
    ];
    public function getCreatedAtAttribute($date)
    {
        return date('Y-m-d', strtotime($date));
    }

    public function getUpdatedAtAttribute($date)
    {
        return date('Y-m-d', strtotime($date));
    }
}
