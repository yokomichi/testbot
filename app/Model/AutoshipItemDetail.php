<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class AutoshipItemDetail extends Model
{
    protected $table        = 'autoship_item_detail';
    protected $primaryKey   = 'id';
    protected $guarded      = ['autoship_id'];
    protected $casts = [
        'autoship_item_id' => 'string',
        'item_id' => 'string',
    ];

}
