<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class AutoshipItemImage extends Model
{
    const UPDATED_AT = null;

    protected $table        = 'autoship_item_image';
    protected $primaryKey   = 'id';
    protected $guarded      = ['autoship_item_id'];
    protected $casts = [
        'autoship_item_id' => 'string',
    ];

    // public function getFileUrlAttribute($path)
    // {
    //     return url('/').$path;
    // }

}
