<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class AutoshipDetail extends Model
{
    const UPDATED_AT = null;

    protected $table        = 'autoship_detail';
    protected $primaryKey   = 'id';
    protected $guarded      = ['autoship_id'];
    protected $casts = [
        'autoship_id' => 'string',
        'item_id' => 'string',
    ];

}
