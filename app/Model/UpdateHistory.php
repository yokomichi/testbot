<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class UpdateHistory extends Model
{
    protected $table        = 'update_history';
    protected $primaryKey   = 'history_id';
    protected $guarded      = ['history_id'];
    public function getCreatedAtAttribute($date)
    {
        return date('Y-m-d', strtotime($date));
    }

    public function getUpdatedAtAttribute($date)
    {
        return date('Y-m-d', strtotime($date));
    }
}
