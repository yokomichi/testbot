<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class AutoshipItemMain extends Model
{
    protected $table        = 'autoship_item_main';
    protected $primaryKey   = 'id';
    protected $guarded      = ['autoship_id'];
    protected $casts = [
        'autoship_item_id' => 'string',
    ];

}
