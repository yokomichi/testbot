<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use SoftDeletes;
class Item extends Model
{
    protected $table        = 'item';
    protected $primaryKey   = 'item_id';
    protected $guarded      = ['item_id'];
    protected $casts = [
        'item_id' => 'string',
    ];
    public function getCreatedAtAttribute($date)
    {
        return date('Y-m-d', strtotime($date));
    }

    public function getUpdatedAtAttribute($date)
    {
        return date('Y-m-d', strtotime($date));
    }

}
