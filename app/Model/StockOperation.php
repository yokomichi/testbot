<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use SoftDeletes;
class StockOperation extends Model
{
    protected $table        = 'stock_operation';
    protected $primaryKey   = 'id';
    protected $guarded      = ['id'];
    public function getCreatedAtAttribute($date)
    {
        return date('Y-m-d', strtotime($date));
    }

    public function getUpdatedAtAttribute($date)
    {
        return date('Y-m-d', strtotime($date));
    }

}
