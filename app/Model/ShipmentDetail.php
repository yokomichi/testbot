<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ShipmentDetail extends Model
{
    const UPDATED_AT = null;

    protected $table        = 'shipment_detail';
    protected $primaryKey   = 'id';
    protected $guarded      = ['shipment_id'];
    protected $casts = [
        'shipment_id' => 'string',
        'item_id' => 'string',
    ];

}
