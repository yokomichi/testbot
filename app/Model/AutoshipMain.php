<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class AutoshipMain extends Model
{
    protected $table        = 'autoship_main';
    protected $primaryKey   = 'id';
    protected $guarded      = ['autoship_id'];
    protected $casts = [
        'autoship_id' => 'string',
    ];

}
