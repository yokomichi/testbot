<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ItemImage extends Model
{
    const UPDATED_AT = null;

    protected $table        = 'item_image';
    protected $primaryKey   = 'id';
    protected $guarded      = ['item_id'];
    protected $casts = [
        'item_id' => 'string',
    ];

    // public function getFileUrlAttribute($path)
    // {
    //     return env('MANAGE_URL').$path;
    // }

}
