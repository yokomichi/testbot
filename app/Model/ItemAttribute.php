<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ItemAttribute extends Model
{
    protected $table        = 'item_attribute';
    protected $primaryKey   = 'id';
    protected $guarded      = ['item_id'];
    protected $casts = [
        'item_id' => 'string',
    ];

}
