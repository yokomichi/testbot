<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class MasterConfigDetail extends Model
{
    protected $table        = 'master_config_detail';
    protected $primaryKey   = 'id';
    protected $guarded      = ['config_id'];
}
