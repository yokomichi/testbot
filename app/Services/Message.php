<?php

namespace App\Services;

class Message {
    public function askAge(): array {
        $array = [
            'type' => 'text',
            'text' => 'あなたの年齢を教えてください',
            'quickReply' => [
                'items' => [
                    [
                        'type' => 'action',
                        'action' => [
                            'type' => 'message',
                            'label' => '20代',
                            'text' => '20代'
                        ]
                    ],
                    [
                        'type' => 'action',
                        'action' => [
                            'type' => 'message',
                            'label' => '30代',
                            'text' => '30代'
                        ]
                    ],
                    [
                        'type' => 'action',
                        'action' => [
                            'type' => 'message',
                            'label' => '40代',
                            'text' => '40代'
                        ]
                    ],
                    [
                        'type' => 'action',
                        'action' => [
                            'type' => 'message',
                            'label' => '50代',
                            'text' => '50代'
                        ]
                    ]
                ]
            ]
        ];
        return $array;
    }
    public function askGender(): array {
        $array = [
            'type' => 'text',
            'text' => 'あなたの性別を教えてください',
            'quickReply' => [
                'items' => [
                    [
                        'type' => 'action',
                        'action' => [
                            'type' => 'message',
                            'label' => 'init',
                            'text' => '男性'
                        ]
                    ],
                    [
                        'type' => 'action',
                        'action' => [
                            'type' => 'message',
                            'label' => '女性',
                            'text' => '女性'
                        ]
                    ]
                ]
            ]
        ];
        return $array;
    }
    public function askCarousel(): array {
        $array = [
            'type' => 'carousel',
            'columns' => [
                'items' => [
                    [
                        "thumbnailImageUrl" => "https://example.com/bot/images/item1.jpg",
                        "imageBackgroundColor" => "#FFFFFF",
                        "title" => "this is menu",
                        "text" => "description",
                        "defaultAction" => [
                            "type" => "uri",
                            "label" => "View detail",
                            "uri" => "http://example.com/page/123"
                        ],
                        "actions" => [
                            [
                                "type" => "postback",
                                "label" => "Buy",
                                "data" => "action=buy&itemid=111"
                            ],
                            [
                                "type" => "postback",
                                "label" => "Add to cart",
                                "data" => "action=add&itemid=111"
                            ],
                            [
                                "type" => "uri",
                                "label" => "View detail",
                                "uri" => "http://example.com/page/111"
                            ]
                        ]
                    ],
                    [
                        "thumbnailImageUrl" => "https://example.com/bot/images/item1.jpg",
                        "imageBackgroundColor" => "#FFFFFF",
                        "title" => "this is menu",
                        "text" => "description",
                        "defaultAction" => [
                            "type" => "uri",
                            "label" => "View detail",
                            "uri" => "http://example.com/page/123"
                        ],
                        "actions" => [
                            [
                                "type" => "postback",
                                "label" => "Buy",
                                "data" => "action=buy&itemid=111"
                            ],
                            [
                                "type" => "postback",
                                "label" => "Add to cart",
                                "data" => "action=add&itemid=111"
                            ],
                            [
                                "type" => "uri",
                                "label" => "View detail",
                                "uri" => "http://example.com/page/111"
                            ]
                        ]
                    ],
                ]
            ]
        ];
        return $array;
    }
}