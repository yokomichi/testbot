<?php

namespace App\Http\Controllers;

use App\Services\Message;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use LINE\LINEBot;
use LINE\LINEBot\Event\MessageEvent\TextMessage;
use LINE\LINEBot\Event\FollowEvent;
use LINE\LINEBot\HTTPClient\CurlHTTPClient;
use LINE\LINEBot\MessageBuilder\RawMessageBuilder;
use LINE\LINEBot\MessageBuilder\TextMessageBuilder;
use LINE\LINEBot\MessageBuilder\TemplateBuilder\CarouselTemplateBuilder;
use LINE\LINEBot\MessageBuilder\MultiMessageBuilder;
use LINE\LINEBot\MessageBuilder\StickerMessageBuilder;
use LINE\LINEBot\MessageBuilder\LocationMessageBuilder;
use LINE\LINEBot\MessageBuilder\TemplateMessageBuilder;
use LINE\LINEBot\MessageBuilder\TemplateBuilder\ConfirmTemplateBuilder;
use LINE\LINEBot\TemplateActionBuilder\MessageTemplateActionBuilder;
use LINE\LINEBot\TemplateActionBuilder\UriTemplateActionBuilder;
use LINE\LINEBot\MessageBuilder\TemplateBuilder\CarouselColumnTemplateBuilder;
use LINE\LINEBot\Event\MessageEvent;
use LINE\LINEBot\Event\MessageEvent\AudioMessage;
use LINE\LINEBot\Event\MessageEvent\ImageMessage;
use LINE\LINEBot\Event\MessageEvent\LocationMessage;
use LINE\LINEBot\Event\MessageEvent\StickerMessage;
use LINE\LINEBot\Event\MessageEvent\UnknownMessage;
use LINE\LINEBot\Event\MessageEvent\VideoMessage;
use LINE\LINEBot\Event\UnfollowEvent;
use App\Model\Member;
use App\Model\MemberCard;
use App\Model\Guest;
use App\Model\MemberTemporary;
use App\Model\MasterConfig;
use App\Model\MasterConfigDetail;
use App\Model\Order;
use App\Model\OrderDetail;
use App\Model\UpdateHistory;
use App\Model\Item;
use App\Model\ItemImage;
use App\Model\ItemDetail;
use App\Model\MemberCart;
use App\Model\Autoship;
use App\Model\AutoshipMain;
use App\Model\AutoshipDetail;
use App\Model\AutoshipItem;
use App\Model\AutoshipItemMain;
use App\Model\AutoshipItemImage;
use App\Model\AutoshipItemDetail;
use App\Model\AutoshipItemInterval;
use App\Model\StockOperation;
use DB;
use Exception;

class ApiController extends Controller
{
    /**
     * @param Request $request
     * @throws \LINE\LINEBot\Exception\CurlExecutionException
     */
    public function webhook(Request $request){
        $httpClient = new CurlHTTPClient(env('LINE_ACCESS_TOKEN'));
        $lineBot = new LINEBot($httpClient, ['channelSecret' => env('LINE_CHANNEL_SECRET')]);

        $signature = $request->header('x-line-signature');

        if (!$lineBot->validateSignature($request->getContent(), $signature)) {
            abort(400, 'Invalid signature');
        }
        $events = $lineBot->parseEventRequest($request->getContent(), $signature);
        foreach ($events as $event) {
            if (!($event instanceof TextMessage)) {
                continue;
            }
            $replyToken = $event->getReplyToken();

            if ($event->getText() === '買い物') {
                $re_type = 'category';
            } else {
                $category_array = DB::table('wp_terms')->join('wp_term_taxonomy', 'wp_term_taxonomy.term_id', '=', 'wp_terms.term_id')->where('wp_term_taxonomy.parent', 7)->where('wp_terms.name', $event->getText())->first();
                if (!empty($category_array)) {
                    $re_type = 'item';
                } else {
                    $re_type = 'chat';
                }
            }

            switch ($re_type) {
                case 'category':
                    $Message = new RawMessageBuilder($this->askCategory());
                    break;
                case 'item':
                    $item = Item::select('item.*', 'item_image.file_url', 'wp_posts.post_name', 'wp_posts.post_title', 'wp_terms.slug', 'item_stock.stock')
                    ->leftjoin('item_image', 'item_image.item_id', '=', 'item.item_id')
                    ->join('wp_posts', 'wp_posts.ID', '=', 'item.item_id')
                    ->join('wp_term_relationships', 'wp_term_relationships.object_id', '=', 'item.item_id')
                    ->join('wp_term_taxonomy', 'wp_term_taxonomy.term_taxonomy_id', '=', 'wp_term_relationships.term_taxonomy_id')
                    ->join('wp_terms', 'wp_terms.term_id', '=', 'wp_term_taxonomy.term_id')
                    ->join('item_stock', function ($q) {
                        $q->on('item_stock.item_id', '=', 'item.item_id')->on('item_stock.shipment_origin_division', '=', 'item.shipment_origin_division');
                    })
                    // ->where('display_status', true)
                    ->where('slug', $category_array->slug)
                    ->get();
                    $columns = [];
                    foreach ($item as $key => $data) {
                        $action = new UriTemplateActionBuilder("購入する", "https://www.momentum-arc.jp/");
                        $column = new CarouselColumnTemplateBuilder($data->post_title, $data->selling_price, 'https://www.momentum-arc.jp/image/base/logo-basic.svg', [$action]);
                        $columns[] = $column;
                    }
                    $carousel = new CarouselTemplateBuilder($columns);
                    $Message = new TemplateMessageBuilder("メッセージのタイトル", $carousel);
                    break;
                default:
                    $Message = new TextMessageBuilder($event->getText());
            }
                    $Message = new TextMessageBuilder($event->getText());
            $lineBot->replyMessage($replyToken, $Message);
        }
    }

    /**
     * @param string $userId
     * @param string $replayToken
     * @throws \LINE\LINEBot\Exception\CurlExecutionException
     */
    private function accountLink($userId, $replayToken){

        $httpClient = new \LINE\LINEBot\HTTPClient\CurlHTTPClient(config("auth.line-api-key"));
        $response = $httpClient->post("https://api.line.me/v2/bot/user/{$userId}/linkToken",[]);

        $rowBody = $response->getRawBody();
        $responseObject = json_decode($rowBody);
        $linkToken = object_get($responseObject, "linkToken");
        $bot = new \LINE\LINEBot($httpClient, ['channelSecret' => config("auth.channel-secret")]);

        $templateMessage = new TemplateMessageBuilder("account link", new ButtonTemplateBuilder("アカウント連携します。", "アカウント連携します。", null, [
            new UriTemplateActionBuilder("OK", route("api.login",["linkToken" => $linkToken]))
        ]));
        $response = $bot->replyMessage($replayToken, $templateMessage);
        $lineResponse = $response->getHTTPStatus() . ' ' . $response->getRawBody();
        \Log::info($lineResponse);
    }

    public function askCategory()
    {
        $category = DB::table('wp_terms')->join('wp_term_taxonomy', 'wp_term_taxonomy.term_id', '=', 'wp_terms.term_id')->where('wp_term_taxonomy.parent', 7)->get();
        $items_array = [];
        foreach ($category as $key => $data) {
            if (strpos($data->name, '全商品') === false) {
                $items_array[] = [
                    'type' => 'action',
                    'action' => [
                        'type' => 'message',
                        'label' => $data->name,
                        'text' => $data->name
                    ]
                ];
            }
        }
       return [
            'type' => 'text',
            'text' => '何をお求めでしょうか？',
            'quickReply' => ['items' => $items_array]
        ];
    }
}
