<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;
use App\Model\Member;
use App\Model\MemberCard;
use App\Model\Guest;
use App\Model\MemberTemporary;
use App\Model\MasterConfig;
use App\Model\MasterConfigDetail;
use App\Model\Order;
use App\Model\OrderDetail;
use App\Model\UpdateHistory;
use App\Model\Item;
use App\Model\ItemImage;
use App\Model\ItemDetail;
use App\Model\MemberCart;
use App\Model\Autoship;
use App\Model\AutoshipMain;
use App\Model\AutoshipDetail;
use App\Model\AutoshipItem;
use App\Model\AutoshipItemMain;
use App\Model\AutoshipItemImage;
use App\Model\AutoshipItemDetail;
use App\Model\AutoshipItemInterval;
use App\Model\StockOperation;
use DB;
use Auth;
use Cookie;
use Illuminate\Support\Facades\Mail;
use App\Mail\Notification;

class AxiosController extends Controller
{
    // public function __construct()
    // {
    //     if (Auth::check()) {
    //         Cookie::queue('la_session', Auth::user()->api_token, 120, null, env('SESSION_DOMAIN'));
    //     }
    // }

    public function setLa_session()
    {
        if (Auth::check()) {
            Cookie::queue('la_session', Auth::user()->api_token, 120, null, env('SESSION_DOMAIN'));
        } else {
            Cookie::queue('la_session', null, time() - 3600);
        }
    }

    public function getSidemenu()
    {
        return DB::table('wp_terms as a')->select('a.slug as first_slug', 'c.name as name', 'c.slug as second_slug')->join('wp_term_taxonomy as b', 'b.parent', '=', 'a.term_id')->join('wp_terms as c', 'c.term_id', '=', 'b.term_id')->where('a.name', '商品ページ')->orderBy('b.count', 'DESC')->get();
    }

    public function getauth(Request $request)
    {
        return Member::select('*')->where('member_id', Auth::id())->first();
    }

    public function getMember(Request $request)
    {
        return [
            'member' => Member::where('member_id', Auth::id())->first(),
            'member_card' => MemberCard::where('member_id', Auth::id())->first(),
        ];
    }

    public function getsession(Request $request)
    {
        return Session::all();
    }
    /**
     * searchMember 会員検索
     *
     * @param Array $request
     * @access protected
     * @return Array
     */

    public function searchMember(Request $request)
    {
        $params = $request->column;
        $query = Member::select('*');
        foreach ($params as $key => $value) {
            // if (strpos($key, 'id') !== false) {
            //     $query->where($key, 'like', '%'.$value);
            // } else if (strpos($key, '_before') !== false) {
            //     $query->where(str_replace('_before', '', $key), '>=', $value);
            // } else if (strpos($key, '_after') !== false) {
            //     $query->where(str_replace('_after', '', $key), '<=', $value);
            // } else {
            //     $query->where($key, '=', $value);
            // }
        }
        $query->orderBy($request->orderby, $request->sort);
        return $query->paginate($request->count);
    }

    /**
     * getConfig 設定取得
     *
     * @param Array $request
     * @access protected
     * @return Array
     */

    public function getConfig()
    {
        $this->setLa_session();
        $array = [];
        $config = MasterConfig::leftjoin('master_config_detail', 'master_config_detail.config_id', '=', 'master_config.config_id')->get()->toArray();
        foreach ($config as $data) $array[$data['config_key']][$data['config_detail_id']] = $data['config_value_1'];
        return ['config' => $array, 'env' => env('MANAGE_URL')];
    }

    public function chengedPassword(Request $request)
    {
        $post_params = $request->register;
        DB::beginTransaction();
        try {
            //foreach ($post_params as $key => $data) {

            $register_table = Member::where('member_id', $post_params['member_id'])->get()->toArray();
            foreach ($register_table as $data) {
                $update_array = $this->updateCode($data, $post_params, 'member', 'member_id');
                if (!empty($update_array)) {
                    Member::where('member_id', $post_params['member_id'])->update($update_array);
                }
            }

            $info = [
                'member_id' => $post_params['member_id'],
                'first_name' => $post_params['first_name'],
                'last_name' => $post_params['last_name'],

            ];
            Mail::to($request->register['mail'])->send(new Notification('ご登録パスワード変更完了のお知らせ', $info, 'mail.edit_password'));
            DB::commit();
            return ['message' => $request->register['mail'].'へ注文完了メールを送信しました！'];
        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
        return [];
    }
    /**
     * registerConfig 設定登録
     *
     * @param Array $request
     * @access protected
     * @return Array
     */

    public function registerConfig(Request $request)
    {
        $post_params = $request->register;

        DB::beginTransaction();
        try {
            $exist_rank = MasterConfigDetail::where('config_id', $post_params['config_id'])->where('rank', $post_params['rank'])->count();
            if ($exist_rank > 0) MasterConfigDetail::where('config_id', $post_params['config_id'])->where('rank', '>=', $post_params['rank'])->increment('rank');
            $register_table = new MasterConfigDetail;
            $register_table->config_id = $post_params['config_id'];
            $register_table->config_detail_id = $post_params['config_detail_id'];
            $register_table->config_value_1 = $post_params['config_value_1'];
            $register_table->rank = $post_params['rank'];
            $register_table->save();

            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
        return [];
    }

    /*
Contract
*/
    public function getforget(Request $request)
    {
        $post_params = $request->register;
        DB::beginTransaction();
        try {
            $register_table = Member::where('mail', $post_params)->first();
            $register_table['password'] = substr(str_shuffle('1234567890abcdefghijklmnopqrstuvwxyz'), 0, 6);
            $register_table_detail = Member::where('mail', $post_params)->get()->toArray();
            foreach ($register_table_detail as $data) {
                $update_array = $this->updateCode($data, $register_table, 'member', 'member_id');
                if (!empty($update_array)) {
                    Member::where('member_id', $post_params['member_id'])->update($update_array);
                }
            }
            $info = [
                'password' => $register_table['password'],
                'member_id' => $register_table['member_id'],
            ];
            Mail::to($post_params)->send(new Notification('パスワード再設定のご案内', $info, 'mail.forgot_account'));
            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
        return [];
    }

    public function getUser(Request $request)
    {
    }
    /**
     * searchConfig 設定検索
     *
     * @param Array $request
     * @access protected
     * @return Array
     */

    public function searchConfig(Request $request)
    {
        $params = $request->column;
        $query = MasterConfig::select('*');
        foreach ($params as $key => $value) {
            $query->where($key, 'like', '%' . $value . '%');
        }
        $query->orderBy($request->orderby, $request->sort);
        return $query->paginate($request->count);
    }

    /**
     * searchConfigDetail 設定詳細検索
     *
     * @param Array $request
     * @access protected
     * @return Array
     */

    public function searchConfigDetail(Request $request)
    {
        return [
            'master_config' => MasterConfig::select('*')->where('config_id', $request->column)->first(),
            'master_detail_config' => MasterConfigDetail::select('*')->where('config_id', $request->column)->orderBy($request->orderby, $request->sort)->get()
        ];
    }

    /**
     * 次回お届け内容API
     */
    protected function getnextOrderDetails(Request $order_id)
    {
        //$order_table = Order::select('*')->where('order.member_id', Auth::id())->get()->first()->toArray();
        $query = OrderDetail::select(
            'order_detail.item_id',
            'order_detail.quantity',
            'order_detail.shipment_hope_date',
            'item.item_main_name'
        )
            ->leftjoin('item', function ($join) {
                $join->on('order_detail.item_id', '=', 'item.item_id');
            });
        $query->where('order_detail.order_id', $order_id);
        $query->orderBy('order_detail.order_detail_id', 'ASC');
        $order_table = $query->get();

        return $order_table;
    }

    /**
     * SaveNextOrder 次回お届け内容変更
     *
     * @param Array $request
     * @access protected
     * @return Array
     */

    public function SaveNextOrder(Request $request)
    {
        $config = $this->getConfig()['config'];
        $register_params = $request->register;
        DB::beginTransaction();
        try {
            $register_table = Order::where('order_id', $register_params['order_id'])->first();
            $register_detail_table = OrderDetail::where('order_id', $register_params['order_id'])->get();
            foreach ($register_table as $data) {
                $update_array = $this->updateCode($data, $register_params, 'order', 'order_id');
                if (!empty($update_array)) {
                    Order::where('order_id', $register_params['order_id'])->update($update_array);
                }
            }

            $info = [
                'config' => $config,
                'member_id' => $register_params['member_id'],
                'order_id' =>$register_params['order_id'],
                'first_name' => $register_params['first_name'],
                'last_name' => $register_params['last_name'],
                'order' => $register_table ,
                'register_detail_params' => $register_detail_table,
            ];
            //var_dump($info['register_detail_table']);exit;
            Mail::to(Auth::user()->mail)->send(new Notification('次回お届け内容の変更完了のお知らせ', $info, 'mail.edit_nextorder'));
            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
        return [];
    }

    // protected function searchOrderDetail()
    // {
    //     $query = Order::select('*');
    //     $query->where('order.member_id', Auth::id());
    //     $query->orderBy('order.order_id', 'DESC');
    //     $order_table = $query->get()->first();
    //     return [
    //         'order' => Order::select('*')->where('order_id', $order_table->order_id)->first(),
    //         'order_detail' => OrderDetail::select('order_detail.*','item.item_main_name', 'item.shipment_origin_division')->where('order_id',$order_table->order_id)->leftjoin('item', 'item.item_id', '=', 'order_detail.item_id')->orderBy('order_id', 'ASC')->get(),
    //     ];
    // }

    /**
     * 次回お届け内容API
     */
    protected function getNextAutoshipOrder()
    {
        $order = [];
        $order_detail = [];
        $order_temp = Order::select('order.order_id')->leftjoin('order_detail', 'order_detail.order_id', '=', 'order.order_id')->where('member_id', Auth::id())->where('shipment_status_division', '<=', 2)->whereNull('cancel_date')->whereNotNull('autoship_id')->oldest()->first();
        if (!empty($order_temp)) $order = Order::where('order_id', $order_temp->order_id)->first();
        if (!empty($order)) $order_detail = OrderDetail::select('order_detail.*', 'wp_posts.post_title')->leftjoin('item', 'item.item_id', '=', 'order_detail.item_id')->leftjoin('wp_posts', 'wp_posts.ID', '=', 'item.item_id')->where('order_id', $order->order_id)->get();
        return [
            'order' => $order,
            'order_detail' => $order_detail,
        ];
    }

    /**
     * 商品取得API
     */
    protected function getProductList(Request $request)
    {
        // Cookie::queue('init_cookie', Auth::user()->api_token, 120, null, env('SESSION_DOMAIN'));
        $query = Item::select('item.*', 'item_image.file_url', 'wp_posts.post_name', 'wp_posts.post_title', 'wp_terms.slug', 'item_stock.stock')
            ->leftjoin('item_image', 'item_image.item_id', '=', 'item.item_id')
            ->join('wp_posts', 'wp_posts.ID', '=', 'item.item_id')
            ->join('wp_term_relationships', 'wp_term_relationships.object_id', '=', 'item.item_id')
            ->join('wp_term_taxonomy', 'wp_term_taxonomy.term_taxonomy_id', '=', 'wp_term_relationships.term_taxonomy_id')
            ->join('wp_terms', 'wp_terms.term_id', '=', 'wp_term_taxonomy.term_id')
            ->join('item_stock', function ($q) {
                $q->on('item_stock.item_id', '=', 'item.item_id')->on('item_stock.shipment_origin_division', '=', 'item.shipment_origin_division');
            })
            ->where('display_status', true)
            ->where('slug', $request->category)
            ->orderBy($request->orderby, $request->sort);
        $item = $query->paginate($request->count);
        return ['item' => $item];
    }

    /**
     * 商品詳細取得API
     */
    protected function getProductDetail(Request $request)
    {
        return [
            'item' => Item::where('item_id', $request->item_id)->first(),
            'item_image' => ItemImage::where('item_id', $request->item_id)->first(),
            'item_detail' => ItemDetail::where('item_id', $request->item_id)->first(),
        ];
    }

    /**
     * 定期商品取得API
     */
    protected function getAutoshipList(Request $request)
    {
        $query = AutoshipItem::select('autoship_item_main.*', 'autoship_item_image.file_url', 'wp_posts.post_name', 'wp_posts.post_title', 'wp_terms.slug')
            ->leftjoin('autoship_item_main', 'autoship_item_main.autoship_item_id', '=', 'autoship_item.autoship_item_id')
            ->leftjoin('autoship_item_image', 'autoship_item_image.autoship_item_id', '=', 'autoship_item.autoship_item_id')
            ->join('wp_posts', 'wp_posts.ID', '=', 'autoship_item.autoship_item_id')
            ->join('wp_term_relationships', 'wp_term_relationships.object_id', '=', 'autoship_item.autoship_item_id')
            ->join('wp_term_taxonomy', 'wp_term_taxonomy.term_taxonomy_id', '=', 'wp_term_relationships.term_taxonomy_id')
            ->join('wp_terms', 'wp_terms.term_id', '=', 'wp_term_taxonomy.term_id')
            ->where('autoship_item_main.autoship_fixed_id', 1)
            ->where('autoship_item.display_status', true)
            ->where('slug', $request->category);
        $query->orderBy($request->orderby, $request->sort);
        $item = $query->paginate($request->count);
        $autoship_item_id = [];
        foreach ($item as $k => $v) $autoship_item_id[$k] = $v->autoship_item_id;

        return [
            'item' => $item,
            'item_detail' => AutoshipItemDetail::select('autoship_item_detail.autoship_item_id','item.selling_price')->leftjoin('item', 'item.item_id', '=', 'autoship_item_detail.item_id')->whereIn('autoship_item_id', $autoship_item_id)->where('autoship_item_detail.autoship_fixed_id', '1')->get(),
        ];
    }

    /**
     * 定期商品詳細取得API
     */
    protected function getAutoshipDetail(Request $request)
    {
        $table = AutoshipItemInterval::where('autoship_item_id', $request->autoship_item_id)->orderBy('shipment_interval','ASC')->get();
        $autoship_item_interval = [];
        foreach ($table as $v) $autoship_item_interval[] = $v->shipment_interval;

        return [
            'autoship_item' => AutoshipItem::where('autoship_item_id', $request->autoship_item_id)->first(),
            'autoship_item_main' => AutoshipItemMain::where('autoship_item_id', $request->autoship_item_id)->where('autoship_item_main.autoship_fixed_id', '1')->first(),
            'autoship_item_image' => AutoshipItemImage::where('autoship_item_id', $request->autoship_item_id)->first(),
            'autoship_item_detail' => AutoshipItemDetail::select('autoship_item_detail.*', 'wp_posts.post_title', 'item_image.file_url')->leftjoin('item', 'item.item_id', '=', 'autoship_item_detail.item_id')->leftjoin('wp_posts', 'wp_posts.ID', '=', 'item.item_id')->leftjoin('item_image', 'item_image.item_id', '=', 'autoship_item_detail.item_id')->where('autoship_item_id', $request->autoship_item_id)->where('autoship_item_detail.autoship_fixed_id', '1')->get(),
            'autoship_item_interval' => $autoship_item_interval,
        ];
    }
    /**
     * SaveAutoship 商品購入保存API
     *
     * @param Array $request
     * @access protected
     * @return Array
     */

    public function SaveAutoship(Request $request) {
        $config = $this->getConfig()['config'];
        $register = $request->register;
        $add_register = $request->add_register;
        $autoship_item = $request->autoship_item;
        $autoship_item_main = $request->autoship_item_main;
        $autoship_item_detail = $request->autoship_item_detail;
        $order_count = Order::where('order_id', 'like', date('ym').'%')->count();
        $order_id = $order_count > 0 ? date('ym').sprintf("%'.04d", $order_count + 1) : date('ym').'0001';
        $credit_info = $request->creditData;
        $member_info = $request->member_info;
        $member_card = $request->member_card;
        $creditRegisterFlag = $request->creditRegisterFlag;
        $nonce = $credit_info['token'];
        $member_id = Auth::id();
        DB::beginTransaction();
        try {
            if ($add_register['payment'] == 1) {
                $access_token = env('SQ_ACCESS_TOKEN');
                $host_url = env('SQ_HOST_URL');
                $api_config = new \SquareConnect\Configuration();
                $api_config->setHost($host_url);
                # Initialize the authorization for Square
                $api_config->setAccessToken($access_token);
                $api_client = new \SquareConnect\ApiClient($api_config);
                $payments_api = new \SquareConnect\Api\PaymentsApi($api_client);
                $customers_api = new \SquareConnect\Api\CustomersApi($api_client);
                if (!$creditRegisterFlag) {
                    /**
                     * 顧客登録
                     */
                    $body = [
                        'given_name' => !empty($member_info) ? $member_info['first_name'] : $register['first_name'],
                        'family_name' => !empty($member_info) ? $member_info['last_name'] : $register['last_name'],
                        'email_address' => !empty($member_info) ? $member_info['mail'] : $register['mail'],
                        'phone_number' => !empty($member_info) ? $member_info['phone'] : $register['phone'],
                        'reference_id' => $order_id.date('YmdHis'),  // customer_id
                    ];
                    $result_customer = $customers_api->createCustomer($body);
                    $first_name = !empty($member_info) ? $member_info['first_name'] : $register['first_name'];
                    $last_name = !empty($member_info) ? $member_info['last_name'] : $register['last_name'];

                    /**
                     *  顧客に紐づくカード登録
                     */
                    $body = [
                        'card_nonce' => $nonce, // フォームのnonce
                        'cardholder_name' => $first_name.' '.$last_name,
                    ];

                    $result_card = $customers_api->createCustomerCard($result_customer['customer']['id'], $body);
                    /**
                     * オーソリー
                     */
                    $body = [
                        'source_id' => $result_card['card']['id'], //$nonce,  // このIDを保存しておく、カード登録しないならフォームの$nonce
                        'idempotency_key' => $order_id.date('YmdHis'), // 一意なID
                        'amount_money' => [
                            'amount' => $autoship_item_main['total'],
                            'currency' => 'JPY'
                        ],
                        'autocomplete' => false, // auth時false、Captureもならtrue,
                        'customer_id' => $result_customer['customer']['id'],     // 支払いに関連付けられた顧客のID。source_idがCustomers APIを使用して作成されたファイル上のカードを参照する場合は必須です。
                        'reference_id' => $order_id.date('YmdHis'),    // order_id

                    ];
                    MemberCard::updateOrCreate(
                        [
                            'member_id' => $member_id,
                            'credit_number' => $credit_info['credit_number']
                        ],
                        [
                            'member_id' => $member_id,
                            'credit_number' => $credit_info['credit_number'],
                            'credit_name' => $result_customer['customer']['id'],
                            'credit_expire_year' => substr($credit_info['exp_year'], 2, 2),
                            'credit_expire_month' => sprintf("%'.02d", $credit_info['exp_month']),
                            'credit_token' => $result_card['card']['id']
                        ]
                    );
                } else {
                    /**
                     * オーソリー
                     */
                    $body = [
                        'source_id' => $member_card['credit_token'], //$nonce,  // このIDを保存しておく、カード登録しないならフォームの$nonce
                        'idempotency_key' => $order_id.date('YmdHis'), // 一意なID
                        'amount_money' => [
                            'amount' => $autoship_item_main['total'],
                            'currency' => 'JPY'
                        ],
                        'autocomplete' => false, // auth時false、Captureもならtrue,
                        'customer_id' => $member_card['credit_name'],     // 支払いに関連付けられた顧客のID。source_idがCustomers APIを使用して作成されたファイル上のカードを参照する場合は必須です。
                        'reference_id' => $order_id.date('YmdHis'),    // order_id

                    ];

                }

                $result_payment = $payments_api->createPayment($body);
            }
            $register_table = new Autoship;
            $register_table->autoship_id = $order_id;
            $register_table->autoship_item_id = $autoship_item['autoship_item_id'];
            $register_table->member_id = $member_id;
            $register_table->company_name = $register['company_name'];
            $register_table->first_name = $register['first_name'];
            $register_table->last_name = $register['last_name'];
            $register_table->first_kana_name = $register['first_kana_name'];
            $register_table->last_kana_name = $register['last_kana_name'];
            $register_table->post = $register['post'];
            $register_table->pref = $register['pref'];
            $register_table->city = $register['city'];
            $register_table->addr = $register['addr'];
            $register_table->building = $register['building'];
            $register_table->mail = $register['mail'];
            $register_table->phone = $register['phone'];
            $register_table->start_date = date('Y-m-d');
            $register_table->shipment_interval = $add_register['shipment_interval'];
            $register_table->shipment_skip = $autoship_item['shipment_skip'];
            $register_table->payment = $add_register['payment'];
            $register_table->order_create_date = date('Y-m-d');
            $register_table->order_create_next_date = date('Y-m-d', strtotime($add_register['shipment_interval']." month"));
            $register_table->save();

            $autoship_main = AutoshipItemMain::where('autoship_item_id', $autoship_item['autoship_item_id'])->get();
            foreach ($autoship_main as $k => $data) {
                $register_table = new AutoshipMain;
                $register_table->autoship_id = $order_id;
                $register_table->autoship_fixed_id = $data['autoship_fixed_id'];
                $register_table->unit_price_total = $data['unit_price_total'];
                $register_table->selling_price_total = $data['selling_price_total'];
                $register_table->discount_rate = $data['discount_rate'];
                $register_table->discount_price = $data['discount_price'];
                $register_table->discount_total = $data['discount_total'];
                $register_table->postage_total = $data['postage_total'];
                $register_table->shipment_price = $add_register['shipment_price'];
                $register_table->total = $data['total'];
                $register_table->timestamps = false;

                $register_table->save();
            }
            $autoship_detail = AutoshipItemDetail::where('autoship_item_id', $autoship_item['autoship_item_id'])->get();
            foreach ($autoship_detail as $k => $data) {
                $register_table = new AutoshipDetail;
                $register_table->autoship_id = $order_id;
                $register_table->autoship_fixed_id = $data['autoship_fixed_id'];
                $register_table->autoship_detail_id = $data['autoship_item_detail_id'];
                $register_table->item_id = $data['item_id'];
                $register_table->unit_price = $data['unit_price'];
                $register_table->selling_price = $data['selling_price'];
                $register_table->detail_total = $data['detail_total'];
                $register_table->discount_rate = $data['discount_rate'];
                $register_table->discount_price = $data['discount_price'];
                $register_table->quantity = $data['quantity'];
                $register_table->postage = $data['postage'];
                $register_table->tax_division = $data['tax_division'];
                $register_table->tax_rate_division = $data['tax_rate_division'];
                $register_table->tax_total = $data['tax_total'];
                $register_table->timestamps = false;
                $register_table->save();
            }
            $order_register_table = new Order;
            $order_register_table->order_id = $order_id.'T1';
            $order_register_table->member_id = $member_id;
            $order_register_table->autoship_id = $order_id;
            $order_register_table->payment_id = isset($result_payment['payment']['id']) ? $result_payment['payment']['id'] : null;
            $order_register_table->company_name = $register['company_name'];
            $order_register_table->first_name = $register['first_name'];
            $order_register_table->last_name = $register['last_name'];
            $order_register_table->first_kana_name = $register['first_kana_name'];
            $order_register_table->last_kana_name = $register['last_kana_name'];
            $order_register_table->post = $register['post'];
            $order_register_table->pref = $register['pref'];
            $order_register_table->city = $register['city'];
            $order_register_table->addr = $register['addr'];
            $order_register_table->building = $register['building'];
            $order_register_table->mail = $register['mail'];
            $order_register_table->phone = $register['phone'];
            $order_register_table->application_date = date('Y-m-d');
            // $order_register_table->actual_date = date('Y-m-d');
            $order_register_table->deposit_date = date('Y-m-d');
            $order_register_table->autoship_date = date('Y-m-d');
            $order_register_table->order_division = 1;
            $order_register_table->payment = $add_register['payment'];
            $order_register_table->slip_division = 2;
            $order_register_table->unit_price_total = $autoship_item_main['unit_price_total'];
            $order_register_table->selling_price_total = $autoship_item_main['selling_price_total'];
            $order_register_table->discount_total = $autoship_item_main['discount_total'];
            $order_register_table->postage_total = $autoship_item_main['postage_total'];
            $order_register_table->shipment_price = $add_register['shipment_price'];
            $order_register_table->total = $autoship_item_main['total'];
            $order_register_table->save();
            foreach ($autoship_item_detail as $k => $v) {
                $item = Item::select('item.*', 'item_stock.stock')->join('item_stock', function ($q) {
                    $q->on('item_stock.item_id', '=', 'item.item_id')->on('item_stock.shipment_origin_division', '=', 'item.shipment_origin_division');
                })->where('item.item_id', $v['item_id'])->first();
                $stock = $item['stock'];
                if ($stock < $v['quantity']) {
                    return [
                        'error' => '数量が在庫を上回っています',
                    ];
                } else {
                    Item::select('item.*', 'item_stock.stock')->join('item_stock', function ($q) {
                        $q->on('item_stock.item_id', '=', 'item.item_id')->on('item_stock.shipment_origin_division', '=', 'item.shipment_origin_division');
                    })->where('item.item_id', $v['item_id'])->decrement('stock', $v['quantity']);
                    $register_table = new StockOperation;
                    $register_table->stock_action_division = 1;
                    $register_table->shipment_origin_division = $item['shipment_origin_division'];
                    $register_table->item_id = $item['item_id'];
                    $register_table->stock = $stock;
                    $register_table->in_stock = 0 - $v['quantity'];
                    $register_table->stock_operation_division = 2;
                    $register_table->user_id = Auth::id();
                    $register_table->user_name = Auth::user()->first_name.' '.Auth::user()->last_name;
                    $register_table->save();
                }
                $order_item_table = new OrderDetail;
                $order_item_table->order_id = $order_id.'T1';
                $order_item_table->order_detail_id = $k + 1;
                $order_item_table->item_id = $v['item_id'];
                $order_item_table->shipment_hope_date = date("Y-m-d", strtotime("5 day"));
                $order_item_table->shipment_hope_time_division = 1;
                $order_item_table->shipment_status_division = 1;
                $order_item_table->unit_price = $v['unit_price'];
                $order_item_table->selling_price = $v['selling_price'];
                $order_item_table->detail_total = $v['detail_total'];
                $order_item_table->quantity = $v['quantity'];
                $order_item_table->postage = $v['postage'];
                $order_item_table->tax_division = $v['tax_division'];
                $order_item_table->tax_rate_division = $v['tax_rate_division'];
                $order_item_table->tax_total = $v['tax_total'];
                $order_item_table->timestamps = false;
                $order_item_table->save();
            }
            $info = [
                'autoship_id' => $order_id,
                'send_info' => $register,
                'config' => $config,
                'add_register' => $add_register,
                'autoship_item' => $autoship_item,
                'autoship_item_main' => $autoship_item_main,
                'autoship_item_detail' => $autoship_item_detail,
                'member_info' => $member_info,
            ];
            Mail::to($member_info['mail'])->send(new Notification('【DEMO SPIDER】定期購入 注文内容ご確認（自動配信メール）', $info, 'mail.autoship_order_complete'));
            DB::commit();
            return ['message' => $member_info['mail'].'へ注文完了メールを送信しました！'];
        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * カート追加API
     */
    protected function AddToCart(Request $request)
    {
        // if (Auth::check()) {
        //     $cart = json_decode(json_encode(MemberCart::where('member_id', Auth::id())->get()), true);
        // } else {
        //     $cart = json_decode(json_encode(MemberCart::where('session', $request->session)->get()), true);
        // }
        if (isset($request->la_session)) {
            $cart = json_decode(json_encode(MemberCart::where('la_session', $request->la_session)->orWhere('wp_session', $request->wp_session)->get()), true);
        } else {
            $cart = json_decode(json_encode(MemberCart::where('wp_session', $request->wp_session)->get()), true);
        }
        $item_array = $request->item;
        if (!empty($cart)) {
            $cart_column = array_column($cart, 'item_id');
            if (in_array($item_array['item_id'], $cart_column)) {
                $index = array_search($item_array['item_id'], $cart_column);
                $cart[$index]['item_count'] += $request->count;
                $item_array = $cart[$index];
                unset($item_array['id']);
                DB::beginTransaction();
                try {
                    if (isset($request->la_session)) {
                        MemberCart::where('la_session', $request->la_session)->orWhere('wp_session', $request->wp_session)->where('item_id', $item_array['item_id'])->delete();
                    } else {
                        MemberCart::where('wp_session', $request->wp_session)->where('item_id', $item_array['item_id'])->delete();
                    }
                    MemberCart::insert($item_array);
                    DB::commit();
                } catch (Exception $e) {
                    DB::rollback();
                    throw $e;
                }
                return '';
            }
        }
        // $item_array['member_id'] = Auth::check() ? Auth::id() : null;
        // $item_array['session'] = Auth::check() ? null : $request->session;
        $item_array['item_count'] = $request->count;
        $item_array['rank'] = count($cart) + 1;
        DB::beginTransaction();
        try {
            $register_table = new MemberCart;
            $register_table->item_id = $item_array['item_id'];
            $register_table->item_main_name = $item_array['post_title'];
            $register_table->stock = $item_array['stock'];
            $register_table->unit_price = $item_array['unit_price'];
            $register_table->selling_price = $item_array['selling_price'];
            $register_table->postage = $item_array['postage'];
            $register_table->tax_division = $item_array['tax_division'];
            $register_table->tax_rate_division = $item_array['tax_rate_division'];
            $register_table->shipment_origin_division = $item_array['shipment_origin_division'];
            $register_table->memo = $item_array['memo'];
            $register_table->wp_session = isset($request->wp_session) ? $request->wp_session : null;
            $register_table->la_session = isset($request->la_session) ? $request->la_session : null;
            $register_table->item_count = $item_array['item_count'];
            $register_table->rank = $item_array['rank'];
            $register_table->timestamps = false;
            $register_table->save();

            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
    }
    /**
     * カート個数取得API
     */
    protected function getItemCount(Request $request)
    {
        if (Auth::check()) {
            $cart_count = MemberCart::where('la_session', $_COOKIE['la_session'])->orWhere('wp_session', $_COOKIE['wp_session'])->sum('item_count');
        } else {
            $cart_count = MemberCart::where('wp_session', $_COOKIE['wp_session'])->sum('item_count');
        }
        return $cart_count;
    }
    /**
     * ユーザーカート取得API
     */
    protected function getUserProduct(Request $request)
    {
        // var_dump($_COOKIE);exit;
        if (Auth::check()) {
            return json_decode(
                    json_encode(
                        MemberCart::select('member_cart.*', 'item_image.file_url', 'item.shipment_origin_division')
                        ->leftjoin('item_image', 'item_image.item_id', '=', 'member_cart.item_id')
                        ->leftjoin('item', 'item.item_id', '=', 'member_cart.item_id')
                        ->where('la_session', $_COOKIE['la_session'])->orWhere('wp_session', $_COOKIE['wp_session'])->orderBy('rank', 'asc')->get()
                    ),true);
        } else {
            return json_decode(
                    json_encode(
                        MemberCart::select('member_cart.*', 'item_image.file_url', 'item.shipment_origin_division')
                        ->leftjoin('item_image', 'item_image.item_id', '=', 'member_cart.item_id')
                        ->leftjoin('item', 'item.item_id', '=', 'member_cart.item_id')
                        ->where('wp_session', $_COOKIE['wp_session'])->orderBy('rank', 'asc')->get()
                    ), true);
        }
    }
    /**
     * ユーザーカート取得API
     */
    protected function SaveProduct(Request $request)
    {
        $item_array = $request->item;
        foreach ($item_array as $k => $v) {
            $item_array[$k]['rank'] = $k + 1;
            unset($item_array[$k]['cart_id']);
            unset($item_array[$k]['file_url']);
        }
        DB::beginTransaction();
        try {
            if (Auth::check()) {
                MemberCart::where('la_session', $_COOKIE['la_session'])->orWhere('wp_session', $_COOKIE['wp_session'])->delete();
            } else {
                MemberCart::where('wp_session', $_COOKIE['wp_session'])->delete();
            }
            MemberCart::insert($item_array);

            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * SaveOrder 商品購入保存API
     *
     * @param Array $request
     * @access protected
     * @return Array
     */

    public function SaveOrder(Request $request) {
        $config = $this->getConfig()['config'];
        $register = $request->register;
        $add_register = $request->add_register;
        $product = $request->product;
        $order_count = Order::where('order_id', 'like', date('ym').'%')->count();
        $order_id = $order_count > 0 ? date('ym').sprintf("%'.04d", $order_count + 1) : date('ym').'0001';
        $credit_info = $request->creditData;
        $member_info = $request->member_info;
        $member_card = $request->member_card;
        $creditRegisterFlag = $request->creditRegisterFlag;
        $guest_id = Guest::max('guest_id') + 1;
        $nonce = $credit_info['token'];
        DB::beginTransaction();
        try {
            if ($add_register['payment'] == 1) {
                $access_token = env('SQ_ACCESS_TOKEN');
                $host_url = env('SQ_HOST_URL');
                $api_config = new \SquareConnect\Configuration();
                $api_config->setHost($host_url);
                # Initialize the authorization for Square
                $api_config->setAccessToken($access_token);
                $api_client = new \SquareConnect\ApiClient($api_config);
                $payments_api = new \SquareConnect\Api\PaymentsApi($api_client);
                $customers_api = new \SquareConnect\Api\CustomersApi($api_client);
                if (!$creditRegisterFlag) {
                    /**
                     * 顧客登録
                     */
                    $body = [
                        'given_name' => !empty($member_info) ? $member_info['first_name'] : $register['first_name'],
                        'family_name' => !empty($member_info) ? $member_info['last_name'] : $register['last_name'],
                        'email_address' => !empty($member_info) ? $member_info['mail'] : $register['mail'],
                        'phone_number' => !empty($member_info) ? $member_info['phone'] : $register['phone'],
                        'reference_id' => $order_id.date('YmdHis'),  // customer_id
                    ];
                    $result_customer = $customers_api->createCustomer($body);
                    $first_name = !empty($member_info) ? $member_info['first_name'] : $register['first_name'];
                    $last_name = !empty($member_info) ? $member_info['last_name'] : $register['last_name'];

                    /**
                     *  顧客に紐づくカード登録
                     */
                    $body = [
                        'card_nonce' => $nonce, // フォームのnonce
                        'cardholder_name' => $first_name.' '.$last_name,
                    ];

                    $result_card = $customers_api->createCustomerCard($result_customer['customer']['id'], $body);
                    /**
                     * オーソリー
                     */
                    $body = [
                        'source_id' => $result_card['card']['id'], //$nonce,  // このIDを保存しておく、カード登録しないならフォームの$nonce
                        'idempotency_key' => $order_id.date('YmdHis'), // 一意なID
                        'amount_money' => [
                            'amount' => $add_register['total'],
                            'currency' => 'JPY'
                        ],
                        'autocomplete' => false, // auth時false、Captureもならtrue,
                        'customer_id' => $result_customer['customer']['id'],     // 支払いに関連付けられた顧客のID。source_idがCustomers APIを使用して作成されたファイル上のカードを参照する場合は必須です。
                        'reference_id' => $order_id.date('YmdHis'),    // order_id

                    ];
                    if (Auth::check()) {
                        MemberCard::updateOrCreate(
                            [
                                'member_id' => Auth::id(),
                                'credit_number' => $credit_info['credit_number']
                            ],
                            [
                                'member_id' => Auth::id(),
                                'credit_number' => $credit_info['credit_number'],
                                'credit_name' => $result_customer['customer']['id'],
                                'credit_expire_year' => substr($credit_info['exp_year'], 2, 2),
                                'credit_expire_month' => sprintf("%'.02d", $credit_info['exp_month']),
                                'credit_token' => $result_card['card']['id']
                            ]
                        );
                    }
                } else {
                    /**
                     * オーソリー
                     */
                    $body = [
                        'source_id' => $member_card['credit_token'], //$nonce,  // このIDを保存しておく、カード登録しないならフォームの$nonce
                        'idempotency_key' => $order_id.date('YmdHis'), // 一意なID
                        'amount_money' => [
                            'amount' => $add_register['total'],
                            'currency' => 'JPY'
                        ],
                        'autocomplete' => false, // auth時false、Captureもならtrue,
                        'customer_id' => $member_card['credit_name'],     // 支払いに関連付けられた顧客のID。source_idがCustomers APIを使用して作成されたファイル上のカードを参照する場合は必須です。
                        'reference_id' => $order_id.date('YmdHis'),    // order_id

                    ];

                }

                $result_payment = $payments_api->createPayment($body);
            }
            $order_register_table = new Order;
            $order_register_table->order_id = $order_id;
            $order_register_table->member_id = Auth::check() ? $member_info['member_id'] : $guest_id;
            $order_register_table->payment_id = isset($result_payment['payment']['id']) ? $result_payment['payment']['id'] : null;
            $order_register_table->company_name = $register['company_name'];
            $order_register_table->first_name = $register['first_name'];
            $order_register_table->last_name = $register['last_name'];
            $order_register_table->first_kana_name = $register['first_kana_name'];
            $order_register_table->last_kana_name = $register['last_kana_name'];
            $order_register_table->post = $register['post'];
            $order_register_table->pref = $register['pref'];
            $order_register_table->city = $register['city'];
            $order_register_table->addr = $register['addr'];
            $order_register_table->building = $register['building'];
            $order_register_table->mail = $register['mail'];
            $order_register_table->phone = $register['phone'];
            $order_register_table->application_date = date('Y-m-d');
            $order_register_table->deposit_date = date('Y-m-d');
            $order_register_table->order_division = 1;
            $order_register_table->payment = $add_register['payment'];
            $order_register_table->slip_division = 2;
            $order_register_table->unit_price_total = $add_register['unit_price_total'];
            $order_register_table->selling_price_total = $add_register['selling_price_total'];
            $order_register_table->discount_total = $add_register['discount_total'];
            $order_register_table->postage_total = $add_register['postage_total'];
            $order_register_table->shipment_price = $add_register['shipment_price'];
            $order_register_table->total = $add_register['total'];
            $order_register_table->save();
            foreach ($product as $k => $v) {
                $tax_total = $v['tax_division'] == 1 ? 0 : floor($v['unit_price'] * $config['tax_rate_division'][$v['tax_rate_division']]);
                $item = Item::select('item.*', 'item_stock.stock')->join('item_stock', function ($q) {
                    $q->on('item_stock.item_id', '=', 'item.item_id')->on('item_stock.shipment_origin_division', '=', 'item.shipment_origin_division');
                })->where('item.item_id', $v['item_id'])->first();
                $stock = $item['stock'];
                if ($stock < $v['item_count']) {
                    return [
                        'error' => '数量が在庫を上回っています',
                    ];
                } else {
                    Item::select('item.*', 'item_stock.stock')->join('item_stock', function ($q) {
                        $q->on('item_stock.item_id', '=', 'item.item_id')->on('item_stock.shipment_origin_division', '=', 'item.shipment_origin_division');
                    })->where('item.item_id', $v['item_id'])->decrement('stock', $v['item_count']);
                    $register_table = new StockOperation;
                    $register_table->stock_action_division = 3;
                    $register_table->shipment_origin_division = $item['shipment_origin_division'];
                    $register_table->item_id = $item['item_id'];
                    $register_table->stock = $stock;
                    $register_table->in_stock = 0 - $v['item_count'];
                    $register_table->stock_operation_division = 2;
                    $register_table->user_id = Auth::id();
                    $register_table->user_name = Auth::user()->first_name.' '.Auth::user()->last_name;
                    $register_table->save();
                }
                $order_item_table = new OrderDetail;
                $order_item_table->order_id = $order_id;
                $order_item_table->order_detail_id = $k + 1;
                $order_item_table->item_id = $v['item_id'];
                $order_item_table->shipment_hope_date = $add_register['shipment_hope_date'];
                $order_item_table->shipment_hope_time_division = $add_register['shipment_hope_time_division'];
                $order_item_table->shipment_status_division = 1;
                $order_item_table->unit_price = $v['unit_price'];
                $order_item_table->selling_price = $v['selling_price'];
                $order_item_table->detail_total = ($v['selling_price'] + $tax_total) * $v['item_count'] + $v['postage'];// - $v['discount_price'];
                $order_item_table->discount_rate = 0;
                $order_item_table->quantity = $v['item_count'];
                $order_item_table->postage = $v['postage'];
                $order_item_table->tax_division = $v['tax_division'];
                $order_item_table->tax_rate_division = $v['tax_rate_division'];
                $order_item_table->tax_total = $tax_total;
                $order_item_table->timestamps = false;

                $order_item_table->save();
            }
            if (Auth::check()) {
                MemberCart::where('member_id', Auth::id())->delete();
            } else {
                MemberCart::where('wp_session', $request->wp_session)->delete();
                $guest_table = new Guest;
                $guest_table->company_name = $member_info['company_name'];
                $guest_table->first_name = $member_info['first_name'];
                $guest_table->last_name = $member_info['last_name'];
                $guest_table->first_kana_name = $member_info['first_kana_name'];
                $guest_table->last_kana_name = $member_info['last_kana_name'];
                $guest_table->post = $member_info['post'];
                $guest_table->pref = $member_info['pref'];
                $guest_table->city = $member_info['city'];
                $guest_table->addr = $member_info['addr'];
                $guest_table->building = $member_info['building'];
                $guest_table->mail = $member_info['mail'];
                $guest_table->phone = $member_info['phone'];
                $guest_table->save();
            }
            $info = [
                'member_id' => Auth::check() ? Auth::id() : $guest_id,
                'config' => $config,
                'order_id' => $order_id,
                'member_info' => $request->member_info,
                'order' => $register,
                'add_register' => $add_register,
                'product' => $product,
            ];
            Mail::to($member_info['mail'])->send(new Notification('【DEMO SPIDER】注文内容ご確認（自動配信メール）', $info, 'mail.order_complete'));
            DB::commit();
            return ['message' => $member_info['mail'].'へ注文完了メールを送信しました！'];

        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }

        /**
         * キャンセル キャンセル可能な決済は'autocomplete' => false（オーソリー状態）のみ
         */
        // $result_cancel = $payments_api->cancelPayment($result_payment['payment']['id']);
        // var_dump($result_cancel);exit;

        /**
         * キャプチャー
         * $result_payment['payment']['id']を保存しておけば、後からキャプチャー出来る
         */
        // $result_complete = $payments_api->completePayment($result_payment['payment']['id'], ['payment_id' => $result_payment['payment']['id']]);

        // var_dump($result_payment);exit;

    }

    /**
     * mailRegister 登録メールを送信
     *
     * @param Array $request
     * @access protected
     * @return Array
     */

    public function mailRegister(Request $request) {
        $url = $request->OrderLinkFlag ? url('/').'/register?mail='.$request->mail.'%26OrderLinkFlag=true' : url('/').'/register?mail='.$request->mail.'%26OrderLinkFlag=false';
        $info = [
            'url' => $url,
        ];
        try {
            $member_register_status = MemberTemporary::select('register_division')->where('mail', $request->mail)->first();
            MemberCart::where('wp_session', $_COOKIE['wp_session'])->update(['mail'=>$request->mail]);
            if (!isset($member_register_status)) {
                Mail::to($request->mail)->send(new Notification('登録メール', $info, 'mail.register'));
                $register_table = new MemberTemporary;
                $register_table->mail = $request->mail;
                $register_table->save();
                return ['register_division'=>0, 'message' => $request->mail.'へ登録メールを送信しました！'];
            }
            if ($member_register_status->register_division === 1) return ['register_division'=>1, 'message' => '仮登録が完了しています 3秒後に自動で登録画面へ遷移します'];
            if ($member_register_status->register_division === 2) return ['register_division'=>2, 'message' => 'すでに登録が完了しています 3秒後に自動でログイン画面へ遷移します'];
        } catch(Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * getMemberTemporary 会員仮登録の取得
     *
     * @param Array $request
     * @access protected
     * @return Array
     */

    public function getMemberTemporary(Request $request) {
        $member = MemberTemporary::where('mail', $request->column)->first();
        if ($member->register_division === 1) return ['count' => 0, 'member' => $member];
        if ($member->register_division === 2) return ['count' => 1, 'member' => []];
    }

    /**
     * saveMemberTemporary 会員仮登録の保存
     *
     * @param Array $request
     * @access protected
     * @return Array
     */

    public function saveMemberTemporary(Request $request) {
        $register_params = $request->register;
        try {
            $register_table = MemberTemporary::where('mail', $register_params['mail'])->get()->toArray();
            foreach ($register_table as $data) {
                $update_array = $this->updateCode($data, $register_params, 'member_temporary', 'id');
                if (!empty($update_array)) {
                    MemberTemporary::where('member_id', $register_params['mail'])->update($update_array);
                }
            }
        } catch(Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * saveMemberTemporary 会員登録の保存
     *
     * @param Array $request
     * @access protected
     * @return Array
     */

    public function saveMember(Request $request) {
        $register_params = $request->register;
        try {
            $api_token = str_random(60);
            Cookie::queue('la_session', $api_token, 120, null, env('SESSION_DOMAIN'));

            $member = Member::where('member_id', 'like', '5'.date('ym').'%')->max('member_id');
            $member_id = isset($member) ? $member + 1 : '5'.date('ym').'1000';
            $register_table = new Member;
            $register_table->org_member_id = $member_id;
            $register_table->member_id = $member_id;
            $register_table->company_name = $register_params['company_name'];
            $register_table->first_name = $register_params['first_name'];
            $register_table->last_name = $register_params['last_name'];
            $register_table->first_kana_name = $register_params['first_kana_name'];
            $register_table->last_kana_name = $register_params['last_kana_name'];
            $register_table->gender = $register_params['gender'];
            $register_table->member_division = $register_params['member_division'];
            $register_table->post = $register_params['post'];
            $register_table->pref = $register_params['pref'];
            $register_table->city = $register_params['city'];
            $register_table->addr = $register_params['addr'];
            $register_table->building = $register_params['building'];
            $register_table->mail = $register_params['mail'];
            $register_table->phone = $register_params['phone'];
            $register_table->password = $register_params['password'];
            $register_table->api_token = $api_token;
            $register_table->withdrawal_division = 1;
            $register_table->save();

            $update = [
                'member_id' => $member_id,
                'company_name' => $register_params['company_name'],
                'first_name' => $register_params['first_name'],
                'last_name' => $register_params['last_name'],
                'first_kana_name' => $register_params['first_kana_name'],
                'last_kana_name' => $register_params['last_kana_name'],
                'gender' => $register_params['gender'],
                'member_division' => $register_params['member_division'],
                'post' => $register_params['post'],
                'pref' => $register_params['pref'],
                'city' => $register_params['city'],
                'addr' => $register_params['addr'],
                'building' => $register_params['building'],
                'mail' => $register_params['mail'],
                'phone' => $register_params['phone'],
                'password' => $register_params['password'],
                'register_division' => 2,
            ];
            $model = MemberTemporary::where('mail', $register_params['mail'])->first();
            $attributes = $model->getAttributes();
            foreach ($attributes as $c => $v) {
                if (!isset($update[$c])) continue;
                $model->$c = $update[$c];
            }
            if ($model->isDirty()) {
                $model->update();
            }
            MemberCart::where('mail', $register_params['mail'])->update(['member_id' => $member_id, 'la_session' => $api_token]);
            if ($request->OrderLinkFlag) {
                $info = [
                    'url' => url('/').'/order',
                    'member_id' => $member_id,
                ];
            } else {
                $info = [
                    'url' => url('/').'/login',
                    'member_id' => $member_id,
                ];
            }
            Mail::to($register_params['mail'])->send(new Notification('登録完了メール', $info, 'mail.register_complete'));

            return ['message' => $register_params['mail'].'へ登録完了メールを送信しました！', 'member_id' => $member_id, 'password' => $register_params['password']];
        } catch(Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * MemberPurchase 購入履歴を取得
     *
     * @param Array $request
     * @access protected
     * @return Array
     */

    protected function getProductHistoryDetail()
    {
        $query = order::select(
            'order.order_id',
            'order.application_date',
            // 'order.actual_date',
            'order.deposit_date',
            'order.order_division',
            'order_detail.quantity',
            'order.payment',
            'item.item_id',
            'item_image.file_url',
            'order_detail.unit_price',
            'wp_posts.post_title'
        );
        $query->leftjoin(
            'order_detail',
            'order_detail.order_id',
            '=',
            'order.order_id'
        );
        $query->leftjoin(
            'item',
            'item.item_id',
            '=',
            'order_detail.item_id'
        );
        $query->leftjoin(
            'item_image',
            'item_image.item_id',
            '=',
            'item.item_id'
        );
        $query->leftjoin(
            'wp_posts',
            'wp_posts.ID',
            '=',
            'item.item_id'
        );
        //出荷済みを表示
        $query->where('order.member_id', Auth::id())->where('order_detail.shipment_status_division',4);
        $query->orderBy('order.order_id', 'DESC');
        $purchase_table = $query->get();
        return $purchase_table;
    }
    /**
     * MemberPurchase 購入履歴を取得
     *
     * @param Array $request
     * @access protected
     * @return Array
     */

    protected function getProductHistory()
    {
        $order_id = [];
        $history = Order::select('*')->where('order.member_id', Auth::id())->orderBy('order.order_id', 'DESC')->paginate(10);
        foreach ($history as $k => $v) $order_id[$k] = $v->order_id;
        return [
            'history' => $history,
            'history_detail' => OrderDetail::select('order_detail.*', 'item_image.file_url', 'wp_posts.post_title')->leftjoin('item_image', 'item_image.item_id', '=', 'order_detail.item_id')->leftjoin('item', 'item.item_id', '=', 'order_detail.item_id')->leftjoin('wp_posts', 'wp_posts.ID', '=', 'item.item_id')->whereIn('order_detail.order_id', $order_id)->orderBy('order_detail.order_id', 'DESC')->get(),
        ];
    }
    /**
     * editMember 会員編集登録
     *
     * @param Array $request
     * @access protected
     * @return Array
     */

    public function editMember(Request $request)
    {
        $post_params = $request->register;
        DB::beginTransaction();
        try {
            $register_table = Member::where('member_id', $post_params['member_id'])->get()->toArray();
            foreach ($register_table as $data) {
                $update_array = $this->updateCode($data, $post_params, 'member', 'member_id');
                if (!empty($update_array)) {
                    Member::where('member_id', $post_params['member_id'])->update($update_array);
                }
            }
            $info = [
                'member_id' => $post_params['member_id'],
                'last_name' => $post_params['last_name'],
                'first_name' => $post_params['last_name'],
            ];
            Mail::to($post_params['mail'])->send(new Notification('ご契約内容の変更完了のお知らせ', $info, 'mail.edit_member'));
            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
        return [];
    }
    /**
     * peggingCart カートのセッション紐付けAPI
     *
     * @param Array $request
     * @access protected
     * @return Array
     */

    public function peggingCart(Request $request)
    {
        Cookie::queue('la_session', Auth::user()->api_token, 120, null, env('SESSION_DOMAIN'));
        if (!isset($_COOKIE['wp_session'])) return [];
        DB::beginTransaction();
        try {
            $member = Member::where('member_id', Auth::id())->first();
            MemberCart::where('wp_session', $_COOKIE['wp_session'])->update(['member_id' => Auth::id(), 'mail' => $member['mail'], 'la_session' => Auth::user()->api_token]);
            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
        return [];
    }

    public function getAutoship(Request $request)
    {
        $params = $request->column;

        $query = Autoship::select('autoship.*', 'autoship_item_image.file_url', 'autoship_item.autoship_item_name')->leftjoin('autoship_item_image', 'autoship_item_image.autoship_item_id', '=', 'autoship.autoship_item_id')->leftjoin('autoship_item', 'autoship_item.autoship_item_id', '=', 'autoship.autoship_item_id')->where('member_id', Auth::id());
        $query->orderBy($request->orderby, $request->sort);
        $item = $query->paginate($request->count);
        return ['item' => $item];
    }

    public function getPurchaseAutoshipDetail(Request $request)
    {
        $autoship = Autoship::where('autoship_id', $request->autoship_id)->first();
        $order_count = Order::where('autoship_id', $request->autoship_id)->count();
        return [
            'autoship' => $autoship,
            'autoship_main' => AutoshipMain::select('autoship_main.*')->where('autoship_id', $request->autoship_id)->where('autoship_id', $request->autoship_id)->where('autoship_fixed_id', $order_count + 1)->first(),
            'autoship_detail' => AutoshipDetail::select('autoship_detail.*', 'item.item_main_name', 'item_image.file_url')->leftjoin('item', 'item.item_id', '=', 'autoship_detail.item_id')->leftjoin('item_image', 'item_image.item_id', '=', 'autoship_detail.item_id')->where('autoship_fixed_id', $order_count + 1)->get(),
            // 'autoship_detail' => AutoshipDetail::select('autoship_detail.*', 'item.item_main_name')->leftjoin('item', 'item.item_id', '=', 'autoship_detail.item_id')->where('autoship_id', $request->autoship_id)->where('autoship_fixed_id', $order_count + 1)->get(),
            'autoship_item_interval' => AutoshipItemInterval::where('autoship_item_id', $autoship['autoship_item_id'])->get(),
            'count' => $order_count,
        ];
    }

    public function savePurchaseAutoshipDetail(Request $request)
    {
        $post_params = $request->register;
        DB::beginTransaction();
        try {
            $register_table = Autoship::where('autoship_id', $post_params['autoship_id'])->get()->toArray();
            foreach ($register_table as $data) {
                $update_array = $this->updateCode($data, $post_params, 'autoship', 'autoship_id');
                if (!empty($update_array)) {
                    Autoship::where('autoship_id', $post_params['autoship_id'])->update($update_array);
                }
            }
            $info = [
                'member_info' => Auth::user(),
                'autoship' => $request->register,
                'autoship_detail' => $request->autoship_detail,
                'config' => $this->getConfig()['config'],
            ];
            Mail::to(Auth::user()->mail)->send(new Notification('【DEMO SPIDER】定期購入 注文内容変更のご確認（自動配信メール）', $info, 'mail.edit_autoship'));
            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
        return [];
    }

    public function skipAutoship(Request $request) {
        DB::beginTransaction();
        try {
            $register_table = Autoship::where('autoship_id', $request->autoship_id)->update(['skip_flag' => true, 'shipment_skip' => $register_table->shipment_skip - 1]);
            // Autoship::where('autoship_id', $request->autoship_id);
            // $this->updateCode($register_table, ['skip_flag' => true, 'shipment_skip' => $register_table->shipment_skip - 1], 'autoship_id');
            // $info = [
            //     'member_id' => $post_params['member_id'],
            //     'last_name' => $post_params['last_name'],
            //     'first_name' => $post_params['last_name'],
            // ];
            // Mail::to($post_params['mail'])->send(new Notification('ご契約内容の変更完了のお知らせ', $info, 'mail.edit_member'));
            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
        return [];
    }

    public function savePayment(Request $request) {
        $credit_info = $request->creditData;
        $user = Auth::user();
        $nonce = $credit_info['token'];
        DB::beginTransaction();
        try {
            $access_token = env('SQ_ACCESS_TOKEN');
            $host_url = env('SQ_HOST_URL');
            $api_config = new \SquareConnect\Configuration();
            $api_config->setHost($host_url);
            # Initialize the authorization for Square
            $api_config->setAccessToken($access_token);
            $api_client = new \SquareConnect\ApiClient($api_config);
            $payments_api = new \SquareConnect\Api\PaymentsApi($api_client);
            $customers_api = new \SquareConnect\Api\CustomersApi($api_client);
                /**
                 * 顧客登録
                 */
                $body = [
                    'given_name' => $user->first_name,
                    'family_name' => $user->last_name,
                    'email_address' => $user->mail,
                    'phone_number' =>  $user->phone,
                    'reference_id' => $user->member_id.date('YmdHis'),  // customer_id
                ];
                $result_customer = $customers_api->createCustomer($body);
                /**
                 *  顧客に紐づくカード登録
                 */
                $body = [
                    'card_nonce' => $nonce, // フォームのnonce
                    'cardholder_name' => $user->first_name.' '.$user->last_name,
                ];

                $result_card = $customers_api->createCustomerCard($result_customer['customer']['id'], $body);
                MemberCard::updateOrCreate(
                    [
                        'member_id' => $user->member_id,
                        'credit_number' => $credit_info['credit_number']
                    ],
                    [
                        'member_id' => $user->member_id,
                        'credit_number' => $credit_info['credit_number'],
                        'credit_name' => $result_customer['customer']['id'],
                        'credit_expire_year' => substr($credit_info['exp_year'], 2, 2),
                        'credit_expire_month' => sprintf("%'.02d", $credit_info['exp_month']),
                        'credit_token' => $result_card['card']['id']
                    ]
                );

            DB::commit();
            return ['message' => $user->mail.'へ注文完了メールを送信しました！'];

        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }

    }
    /**
     * updateCode
     * 操作履歴保存処理
     */
    public function updateCode($original, $replica, $history_table, $id, $detail_id = null)
    {
        $insert_array = [];
        $update_array = [];
        if (Auth::check()) {
            $auth_id = Auth::id();
            $auth_name = Auth::user()->first_name.' '.Auth::user()->last_name;
        } else {
            $auth_id = '';
            $auth_name = 'guest';
        }
        foreach ($original as $key => $data) {
            if ($key !== 'created_at' && $key !== 'updated_at' && $key !== 'deleted_at') {
                $replica[$key] = $replica[$key] === 'null' ? null : $replica[$key];
                if ($data != $replica[$key]) {
                    $update_array[$key] = $replica[$key];
                    $insert_array[] = [
                        'field_id' => $replica[$id],
                        'field_detail_id' => $replica[$detail_id],
                        'field_name' => $key,
                        'before_value' => $data,
                        'after_value' => $replica[$key],
                        'user_id' => $auth_id,
                        'user_name' => $auth_name,
                        'created_at' => now(),
                        'updated_at' => now(),
                    ];
                }
            }
        }
        if (!empty($insert_array)) {
            DB::table('update_history_'.$history_table)->insert($insert_array);
        }
        return $update_array;
    }

}
