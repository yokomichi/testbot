<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CMemberCart extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('member_cart');

        Schema::create('member_cart', function (Blueprint $table) {
            $table->increments('cart_id');
            $table->string('item_id',12)->comment('商品番号');
            $table->string('item_main_name',100)->comment('メイン商品名');
            $table->string('item_sub_name',100)->nullable()->comment('サブ商品名');
            $table->integer('stock')->default(0)->comment('在庫');
            $table->integer('unit_price')->comment('単価');
            $table->integer('commission')->default(0)->comment('コミッション');
            $table->integer('selling_price')->comment('販売価格');
            $table->integer('postage')->default(0)->comment('送料');
            $table->integer('tax_division')->comment('税区分');
            $table->integer('tax_rate_division')->comment('税率');
            $table->integer('item_type')->comment('商品タイプ');
            $table->integer('attribute_division')->comment('属性区分');
            $table->integer('shipment_origin_division')->comment('発送元');
            $table->date('sale_start_date')->nullable()->comment('販売開始日');
            $table->date('sale_end_date')->nullable()->comment('販売終了日');
            $table->text('memo')->nullable()->comment('備考');
            $table->string('member_id',12)->nullable()->comment('会員番号');
            $table->string('mail', 100)->nullable()->comment('メールアドレス');
            $table->string('session', 255)->nullable()->comment('セッションID');
            $table->integer('item_count')->default(0)->comment('商品個数');
            $table->integer('rank')->default(0)->comment('順番');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('member_cart');
    }
}
