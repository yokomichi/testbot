<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAutoship extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('autoship', function (Blueprint $table) {
            $table->integer('discount_rate')->default(0)->after('selling_price_total')->comment('割引率');
            $table->integer('discount_price')->default(0)->after('discount_rate')->comment('割引額');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('autoship', function (Blueprint $table) {
            $table->dropColumn('discount_rate');
            $table->dropColumn('discount_price');
        });
    }
}
