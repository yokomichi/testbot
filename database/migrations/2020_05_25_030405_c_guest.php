<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CGuest extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('guest');

        Schema::create('guest', function (Blueprint $table) {
            $table->increments('guest_id')->comment('ゲスト番号');
            $table->string('company_name',100)->nullable()->comment('会社名');
            $table->string('first_name',50)->comment('姓');
            $table->string('last_name',50)->comment('名');
            $table->string('first_kana_name',100)->comment('姓(カタカナ)');
            $table->string('last_kana_name',100)->comment('名(カタカナ)');
            $table->string('post',8)->comment('郵便番号');
            $table->integer('pref')->comment('都道府県');
            $table->string('city',100)->comment('市区町村');
            $table->string('addr',100)->comment('番地');
            $table->string('building',100)->nullable()->comment('建物名');
            $table->string('mail',100)->comment('メールアドレス');
            $table->string('phone',20)->comment('電話番号');
            $table->timestamps();
            $table->softDeletes();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('guest');
    }
}
