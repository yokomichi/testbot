<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CStockOperation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('stock_operation');

        Schema::create('stock_operation', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('stock_action_division')->comment('在庫操作区分');
            $table->integer('shipment_origin_division')->comment('発送元区分');
            $table->string('item_id', 12)->comment('商品番号');
            $table->integer('stock')->comment('在庫');
            $table->integer('in_stock')->comment('入庫');
            $table->integer('stock_operation_division')->comment('入出庫区分');
            $table->string('user_id', 255)->comment('ユーザID');
            $table->string('user_name', 255)->comment('ユーザ名');
            $table->timestamps();
            $table->softDeletes();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stock_operation');
    }
}
