<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DropAutoship extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('autoship', function (Blueprint $table) {
            $table->dropColumn('item_id');
            $table->dropColumn('application_date');
        });
        Schema::table('autoship', function (Blueprint $table) {
            $table->string('autoship_item_id', 12)->after('autoship_id')->comment('定期商品番号');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
