<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterItemDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('item_detail', function (Blueprint $table) {
            $table->integer('rank')->nullable()->after('item_comment')->comment('順番');
        });
        Schema::table('item_image', function (Blueprint $table) {
            $table->dropColumn('image_division');
        });
        Schema::table('item_image', function (Blueprint $table) {
            $table->integer('rank')->nullable()->comment('順番');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('item_detail', function (Blueprint $table) {
            $table->dropColumn('rank');
        });
    }
}
