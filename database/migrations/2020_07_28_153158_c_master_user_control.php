<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CMasterUserControl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('master_user_control');
        Schema::create('master_user_control', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('role')->comment('権限');
            $table->string('name', 255)->comment('名称');
            $table->string('type', 255)->comment('制御タイプ');
            $table->string('upper', 255)->nullable()->comment('上');
            $table->string('middle', 255)->nullable()->comment('中');
            $table->string('lower', 255)->nullable()->comment('下');
            $table->boolean('display_status')->default(false)->comment('表示ステータス');
            $table->timestamps();
            $table->softDeletes();
        });
        Schema::dropIfExists('master_control');
        Schema::create('master_control', function (Blueprint $table) {
            $table->increments('id');
            $table->string('parent_name', 255)->comment('親名称');
            $table->string('name', 255)->comment('名称');
            $table->string('type', 255)->comment('制御タイプ');
            $table->string('upper', 255)->nullable()->comment('上');
            $table->string('middle', 255)->nullable()->comment('中');
            $table->string('lower', 255)->nullable()->comment('下');
            $table->boolean('display_status')->default(false)->comment('表示ステータス');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('master_user_control');
        Schema::dropIfExists('master_control');
    }
}
