<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DropItem2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('item', function (Blueprint $table) {
            $table->dropColumn('item_sub_name');
            $table->dropColumn('item_type');
            $table->dropColumn('commission');
            $table->dropColumn('attribute_division');
        });
        Schema::table('member_cart', function (Blueprint $table) {
            $table->dropColumn('item_sub_name');
            $table->dropColumn('commission');
            $table->dropColumn('item_type');
            $table->dropColumn('attribute_division');
            $table->dropColumn('sale_start_date');
            $table->dropColumn('sale_end_date');
        });
        Schema::table('order', function (Blueprint $table) {
            $table->dropColumn('commission_total');
            $table->dropColumn('actual_date');
        });
        Schema::table('order_detail', function (Blueprint $table) {
            $table->dropColumn('commission');
        });
        Schema::table('shipment', function (Blueprint $table) {
            $table->dropColumn('commission_total');
        });
        Schema::table('shipment_detail', function (Blueprint $table) {
            $table->dropColumn('commission');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
