<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DropItem3 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('item', function (Blueprint $table) {
            $table->date('sale_start_date')->nullable()->after('shipment_origin_division')->comment('販売開始日');
            $table->date('sale_end_date')->nullable()->after('sale_start_date')->comment('販売終了日');
            $table->dropColumn('stock');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('item', function (Blueprint $table) {
            $table->dropColumn('sale_start_date');
            $table->dropColumn('sale_end_date');
        });
}
}
