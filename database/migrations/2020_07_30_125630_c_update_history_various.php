<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CUpdateHistoryVarious extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('update_history');
        $array = [
            'autoship',
            'autoship_detail',
            'autoship_item',
            'autoship_item_detail',
            'autoship_item_image',
            'autoship_item_interval',
            'autoship_item_main',
            'autoship_main',
            'item',
            'item_detail',
            'item_image',
            'master_config',
            'master_config_detail',
            'master_control',
            'master_user',
            'master_user_control',
            'member',
            'member_card',
            'member_temporary',
            'order',
            'order_detail',
            'shipment',
            'shipment_detail',
        ];
        foreach ($array as $data) {
            Schema::dropIfExists('update_history_'.$data);

            Schema::create('update_history_'.$data, function (Blueprint $table) {
                $table->increments('id');
                $table->string('field_name', 255)->comment('カラム名');
                $table->string('before_value', 255)->nullable()->comment('変更前値');
                $table->string('after_value', 255)->nullable()->comment('変更後値');
                $table->string('user_id', 255)->comment('変更者ID');
                $table->string('user_name', 255)->comment('変更者名');
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $array = [
            'autoship',
            'autoship_detail',
            'autoship_item',
            'autoship_item_detail',
            'autoship_item_image',
            'autoship_item_interval',
            'autoship_item_main',
            'autoship_main',
            'item',
            'item_detail',
            'item_image',
            'master_config',
            'master_config_detail',
            'master_control',
            'master_user',
            'master_user_control',
            'member',
            'member_card',
            'member_temporary',
            'order',
            'order_detail',
            'shipment',
            'shipment_detail',
        ];
        foreach ($array as $data) {
            Schema::dropIfExists('update_history_'.$data);
        }
    }
}
