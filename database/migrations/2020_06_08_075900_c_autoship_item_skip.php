<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CAutoshipItemSkip extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('autoship_item', function (Blueprint $table) {
            $table->integer('shipment_skip')->default(0)->after('sale_end_date')->comment('スキップ上限');
        });
        Schema::table('autoship', function (Blueprint $table) {
            $table->integer('shipment_skip')->default(0)->after('shipment_interval')->comment('スキップ数');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('autoship_item', function (Blueprint $table) {
            $table->dropColumn('shipment_skip');
        });
        Schema::table('autoship', function (Blueprint $table) {
            $table->dropColumn('shipment_skip');
        });
    }
}
