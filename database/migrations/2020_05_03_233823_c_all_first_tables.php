<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CAllFirstTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        /**
         * ログイン用テーブル
         */

        Schema::dropIfExists('master_user');

        Schema::create('master_user', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('status')->default(1)->comment('利用状況(0=停止中,1=利用中)');
            $table->string('login_id', 50)->unique()->comment('ログイン用ID');
            $table->string('first_name', 100)->comment('姓');
            $table->string('last_name', 100)->comment('名');
            $table->string('password')->comment('パスワード');
            $table->integer('role')->comment('権限');
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
        });
        $tablename = 'master_user';
        DB::table($tablename)->insert( array( 'id' => '1', 'status' => '1', 'login_id' => 'yokomichi', 'first_name' => '横道', 'last_name' => '睦月', 'password' => '0000', 'role' => 9));

        /**
         * Member関連テーブル
         */

        Schema::dropIfExists('member');

        Schema::create('member', function (Blueprint $table) {
            $table->string('org_member_id',12)->comment('オリジナル会員番号');
            $table->string('member_id',9)->primary()->comment('会員番号');
            $table->string('company_name',100)->nullable()->comment('会社名');
            $table->string('first_name',50)->comment('姓');
            $table->string('last_name',50)->comment('名');
            $table->string('first_kana_name',100)->comment('姓(カタカナ)');
            $table->string('last_kana_name',100)->comment('名(カタカナ)');
            $table->integer('gender')->comment('性別');
            $table->integer('member_division')->comment('会員区分');
            // $table->integer('payment')->comment('支払方法');
            $table->string('post',8)->comment('郵便番号');
            $table->integer('pref')->comment('都道府県');
            $table->string('city',100)->comment('市区町村');
            $table->string('addr',100)->comment('番地');
            $table->string('building',100)->nullable()->comment('建物名');
            $table->string('mail',100)->comment('メールアドレス');
            $table->string('phone',20)->comment('電話番号');
            $table->string('credit_number',4)->nullable()->comment('クレジットカード番号');
            $table->string('credit_name',100)->nullable()->comment('クレジットカード名義');
            $table->string('credit_expire_year',2)->nullable()->comment('クレジットカード有効期限年');
            $table->string('credit_expire_month',2)->nullable()->comment('クレジットカード有効期限月');
            $table->string('credit_token',100)->nullable()->comment('クレジットトークン');
            $table->string('password',20)->comment('パスワード');
            $table->integer('withdrawal_division')->comment('退会区分');
            $table->date('withdrawal_date')->nullable()->comment('退会日');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::dropIfExists('member_invoice');

        Schema::create('member_invoice', function (Blueprint $table) {
            $table->increments('id');
            $table->string('member_id',9)->comment('会員番号');
            $table->string('invoice_url',255)->comment('請求書URL');
            $table->text('invoice_comment')->nullable()->comment('請求書説明');
            $table->timestamps();
        });


        /**
         * Order関連テーブル
         */

        Schema::dropIfExists('order');

        Schema::create('order', function (Blueprint $table) {
            $table->string('order_id',12)->primary()->comment('受注番号');
            $table->string('member_id',9)->nullable()->comment('会員番号');
            $table->string('autoship_id',12)->nullable()->comment('定期番号');
            $table->string('company_name',100)->nullable()->comment('会社名');
            $table->string('first_name',50)->comment('姓');
            $table->string('last_name',50)->comment('名');
            $table->string('first_kana_name',100)->comment('姓(カタカナ)');
            $table->string('last_kana_name',100)->comment('名(カタカナ)');
            $table->string('post',8)->comment('郵便番号');
            $table->integer('pref')->comment('都道府県');
            $table->string('city',100)->comment('市区町村');
            $table->string('addr',100)->comment('番地');
            $table->string('building',100)->nullable()->comment('建物名');
            $table->string('mail',100)->comment('メールアドレス');
            $table->string('phone',20)->comment('電話番号');
            $table->date('application_date')->comment('申込日');
            $table->date('actual_date')->comment('実績日');
            $table->date('deposit_date')->nullable()->comment('入金日');
            $table->date('autoship_date')->nullable()->comment('定期売上作成日');
            $table->integer('order_division')->comment('受注区分');
            $table->integer('payment')->comment('支払方法');
            $table->integer('slip_division')->comment('伝票区分');
            $table->integer('unit_price_total')->comment('単価価格合計');
            $table->integer('commission_total')->default(0)->comment('コミッション合計');
            $table->integer('selling_price_total')->comment('販売価格合計');
            $table->integer('discount_total')->default(0)->comment('割引額合計');
            $table->integer('postage_total')->default(0)->comment('送料合計');
            $table->integer('shipment_price')->default(0)->comment('代引手数料');
            $table->integer('total')->comment('合計金額');
            $table->date('cancel_date')->nullable()->comment('キャンセル日');
            $table->text('memo')->nullable()->comment('備考');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::dropIfExists('order_detail');

        Schema::create('order_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->string('order_id',12)->comment('受注番号');
            $table->integer('order_detail_id')->comment('売上詳細番号');
            $table->string('item_id',12)->comment('商品番号');
            $table->date('shipment_hope_date')->comment('出荷希望日');
            $table->integer('shipment_hope_time_division')->comment('出荷希望時間帯区分');
            $table->integer('shipment_status_division')->comment('出荷状況');
            $table->integer('unit_price')->comment('単価');
            $table->integer('selling_price')->comment('販売価格');
            $table->integer('detail_total')->comment('詳細合計金額');
            $table->integer('commission')->default(0)->comment('コミッション');
            $table->integer('discount_rate')->default(0)->comment('割引率');
            $table->integer('discount_price')->default(0)->comment('割引額');
            $table->integer('quantity')->comment('数量');
            $table->integer('postage')->default(0)->comment('送料');
            $table->integer('tax_division')->comment('税区分');
            $table->integer('tax_rate_division')->comment('税率区分');
            $table->integer('tax_total')->comment('合計税額');
        });

        /**
         * Shipment関連テーブル
         */

        Schema::dropIfExists('shipment');

        Schema::create('shipment', function (Blueprint $table) {
            $table->string('shipment_id',12)->primary()->comment('出荷番号');
            $table->integer('shipment_id_for_date')->comment('対日出荷番号');
            $table->string('member_id',9)->nullable()->comment('会員番号');
            $table->string('order_id',12)->comment('受注番号');
            $table->string('company_name',100)->nullable()->comment('会社名');
            $table->string('first_name',50)->comment('姓');
            $table->string('last_name',50)->comment('名');
            $table->string('first_kana_name',100)->comment('姓(カタカナ)');
            $table->string('last_kana_name',100)->comment('名(カタカナ)');
            $table->string('post',8)->comment('郵便番号');
            $table->integer('pref')->comment('都道府県');
            $table->string('city',100)->comment('市区町村');
            $table->string('addr',100)->comment('番地');
            $table->string('building',100)->nullable()->comment('建物名');
            $table->string('mail',100)->comment('メールアドレス');
            $table->string('phone',20)->comment('電話番号');
            $table->string('shipment_number',50)->nullable()->comment('出荷番号');
            $table->integer('payment')->comment('支払方法');
            $table->integer('unit_price_total')->comment('単価価格合計');
            $table->integer('commission_total')->default(0)->comment('コミッション合計');
            $table->integer('selling_price_total')->comment('販売価格合計');
            $table->integer('discount_total')->default(0)->comment('割引額合計');
            $table->integer('postage_total')->default(0)->comment('送料合計');
            $table->integer('shipment_price')->default(0)->comment('代引手数料');
            $table->integer('total')->comment('合計金額');
            $table->text('memo')->nullable()->comment('備考');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::dropIfExists('shipment_detail');

        Schema::create('shipment_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->string('shipment_id',12)->comment('受注番号');
            $table->integer('shipment_detail_id')->comment('売上詳細番号');
            $table->string('item_id',12)->comment('商品番号');
            $table->date('shipment_hope_date')->comment('出荷希望日');
            $table->integer('shipment_hope_time_division')->comment('出荷希望時間帯区分');
            $table->date('shipment_request_date')->nullable()->comment('出荷依頼日');
            $table->date('shipment_complete_date')->nullable()->comment('出荷完了日');
            $table->integer('shipment_status_division')->comment('出荷状況');
            $table->string('shipment_number',50)->nullable()->comment('配達番号');
            $table->integer('unit_price')->comment('単価');
            $table->integer('selling_price')->comment('販売価格');
            $table->integer('detail_total')->comment('詳細合計金額');
            $table->integer('commission')->default(0)->comment('コミッション');
            $table->integer('discount_rate')->default(0)->comment('割引率');
            $table->integer('discount_price')->default(0)->comment('割引額');
            $table->integer('quantity')->comment('数量');
            $table->integer('postage')->default(0)->comment('送料');
            $table->integer('tax_division')->comment('税区分');
            $table->integer('tax_rate_division')->comment('税率区分');
            $table->integer('tax_total')->comment('合計税額');
        });

        /**
         * Autoship関連テーブル
         */

        Schema::dropIfExists('autoship');

        Schema::create('autoship', function (Blueprint $table) {
            $table->string('autoship_id',12)->primary()->comment('受注番号');
            $table->string('member_id',9)->comment('会員番号');
            $table->string('item_id',12)->comment('定期番号');
            $table->string('company_name',100)->nullable()->comment('会社名');
            $table->string('first_name',50)->comment('姓');
            $table->string('last_name',50)->comment('名');
            $table->string('first_kana_name',100)->comment('姓(カタカナ)');
            $table->string('last_kana_name',100)->comment('名(カタカナ)');
            $table->string('post',8)->comment('郵便番号');
            $table->integer('pref')->comment('都道府県');
            $table->string('city',100)->comment('市区町村');
            $table->string('addr',100)->comment('番地');
            $table->string('building',100)->nullable()->comment('建物名');
            $table->string('mail',100)->comment('メールアドレス');
            $table->string('phone',20)->comment('電話番号');
            $table->date('application_date')->comment('申込日');
            $table->date('start_date')->comment('開始日');
            $table->integer('payment')->comment('支払方法');
            $table->date('sleep_start_date')->nullable()->comment('休止開始日');
            $table->date('sleep_end_date')->nullable()->comment('休止終了日');
            $table->date('order_create_date')->comment('受注データ作成日');
            $table->integer('unit_price_total')->comment('単価価格合計');
            $table->integer('commission_total')->default(0)->comment('コミッション合計');
            $table->integer('selling_price_total')->comment('販売価格合計');
            $table->integer('discount_total')->default(0)->comment('割引額合計');
            $table->integer('postage_total')->default(0)->comment('送料合計');
            $table->integer('shipment_price')->default(0)->comment('代引手数料');
            $table->integer('total')->comment('合計金額');
            $table->date('cancel_date')->nullable()->comment('キャンセル日');
            $table->text('memo')->nullable()->comment('備考');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::dropIfExists('autoship_detail');

        Schema::create('autoship_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->string('autoship_id',12)->comment('受注番号');
            $table->integer('autoship_detail_id')->comment('売上詳細番号');
            $table->string('item_id',12)->comment('商品番号');
            $table->integer('unit_price')->comment('単価');
            $table->integer('selling_price')->comment('販売価格');
            $table->integer('detail_total')->comment('詳細合計金額');
            $table->integer('commission')->default(0)->comment('コミッション');
            $table->integer('discount_rate')->default(0)->comment('割引率');
            $table->integer('discount_price')->default(0)->comment('割引額');
            $table->integer('quantity')->comment('数量');
            $table->integer('postage')->default(0)->comment('送料');
            $table->integer('tax_division')->comment('税区分');
            $table->integer('tax_rate_division')->comment('税率区分');
            $table->integer('tax_total')->comment('合計税額');
        });

        Schema::dropIfExists('autoship_item');

        Schema::create('autoship_item', function (Blueprint $table) {
            $table->increments('id');
            $table->string('autoship_item_id',12)->comment('定期商品番号');
            $table->string('autoship_item_name',255)->comment('定期商品名');
            $table->text('autoship_item_comment')->nullable()->comment('備考');
            $table->date('sale_start_date')->nullable()->comment('販売開始日');
            $table->date('sale_end_date')->nullable()->comment('販売終了日');
            $table->integer('discount_rate')->comment('割引率');
            $table->integer('discount_price')->default(0)->comment('割引額');
            $table->integer('unit_price_total')->comment('単価価格合計');
            $table->integer('commission_total')->default(0)->comment('コミッション合計');
            $table->integer('selling_price_total')->comment('販売価格合計');
            $table->integer('discount_total')->default(0)->comment('割引額合計');
            $table->integer('postage_total')->default(0)->comment('送料合計');
            $table->integer('shipment_price')->default(0)->comment('代引手数料');
            $table->integer('total')->comment('合計金額');
            $table->text('memo')->nullable()->comment('備考');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::dropIfExists('autoship_item_detail');

        Schema::create('autoship_item_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->string('autoship_item_id',12)->comment('受注番号');
            $table->integer('autoship_item_detail_id')->comment('売上詳細番号');
            $table->string('item_id',12)->comment('商品番号');
            $table->integer('unit_price')->comment('単価');
            $table->integer('selling_price')->comment('販売価格');
            $table->integer('detail_total')->comment('詳細合計金額');
            $table->integer('commission')->default(0)->comment('コミッション');
            $table->integer('discount_rate')->default(0)->comment('割引率');
            $table->integer('discount_price')->default(0)->comment('割引額');
            $table->integer('quantity')->comment('数量');
            $table->integer('postage')->default(0)->comment('送料');
            $table->integer('tax_division')->comment('税区分');
            $table->integer('tax_rate_division')->comment('税率区分');
            $table->integer('tax_total')->comment('合計税額');
        });

        /**
         * Documentテーブル
         */

        Schema::dropIfExists('document');

        Schema::create('document', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('document_division')->comment('ドキュメント区分');
            $table->string('document_name',100)->comment('ドキュメント名');
            $table->text('document_comment')->nullable()->comment('ドキュメント説明');
            $table->string('document_url',100)->comment('ドキュメントURL');
            $table->date('start_date')->nullable()->comment('開始日');
            $table->date('end_date')->nullable()->comment('終了日');
            $table->integer('rank')->default(1)->comment('順番');
            $table->timestamps();
            $table->softDeletes();
        });

        /**
         * update_historyテーブル
         */

        Schema::dropIfExists('update_history');

        Schema::create('update_history', function (Blueprint $table) {
            $table->increments('history_id')->comment('履歴番号');
            $table->string('id_type',50)->comment('IDタイプ');
            $table->string('id',50)->comment('ID');
            $table->string('table_name',100)->comment('テーブル名');
            $table->string('field_name',100)->comment('カラム名');
            $table->string('before_value',255)->nullable()->comment('変更前値');
            $table->string('after_value',255)->nullable()->comment('変更後値');
            $table->string('user_id',50)->comment('ユーザID');
            $table->timestamps();
        });

        /**
         * Item関連テーブル
         */

        Schema::dropIfExists('item');

        Schema::create('item', function (Blueprint $table) {
            $table->string('item_id',12)->primary()->comment('商品番号');
            $table->string('item_main_name',100)->comment('メイン商品名');
            $table->string('item_sub_name',100)->nullable()->comment('サブ商品名');
            $table->integer('stock')->default(0)->comment('在庫');
            $table->integer('unit_price')->comment('単価');
            $table->integer('commission')->default(0)->comment('コミッション');
            $table->integer('selling_price')->comment('販売価格');
            $table->integer('postage')->default(0)->comment('送料');
            $table->integer('tax_division')->comment('税区分');
            $table->integer('tax_rate_division')->comment('税率');
            $table->integer('item_type')->comment('商品タイプ');
            $table->integer('attribute_division')->comment('属性区分');
            $table->date('sale_start_date')->nullable()->comment('販売開始日');
            $table->date('sale_end_date')->nullable()->comment('販売終了日');
            $table->text('memo')->nullable()->comment('備考');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::dropIfExists('item_detail');

        Schema::create('item_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->string('item_id',12)->comment('商品番号');
            $table->text('item_comment')->nullable()->comment('商品説明');
        });

        Schema::dropIfExists('item_image');

        Schema::create('item_image', function (Blueprint $table) {
            $table->increments('id');
            $table->string('item_id',12)->comment('商品番号');
            $table->string('file_url',255)->comment('画像URL');
            $table->integer('image_division')->comment('画像区分');
        });

        Schema::dropIfExists('tci');

        Schema::create('tci', function (Blueprint $table) {
            $table->increments('tci_id');
            $table->string('member_id', 9)->comment('会員番号');
            $table->integer('reciept_user_id')->nullable()->comment('受付担当ID');
            $table->datetime('reciept_datetime')->nullable()->comment('受付日時');
            $table->integer('reciept_type')->comment('受付種類');
            $table->text('inquiry')->nullable()->comment('受付内容');
            $table->text('answer')->nullable()->comment('回答内容');
            $table->integer('answer_user_id')->nullable()->comment('回答担当ID');
            $table->datetime('answer_datetime')->nullable()->comment('回答日時');
            $table->integer('end_status')->comment('対応状況');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::dropIfExists('master_config');

        Schema::create('master_config', function (Blueprint $table) {
            $table->increments('config_id');
            $table->string('config_key', 50)->comment('設定キー');
            $table->string('config_name', 50)->comment('設定名');
            $table->string('config_comment', 255)->comment('設定説明');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::dropIfExists('master_config_detail');

        Schema::create('master_config_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('config_id')->comment('設定番号');
            $table->integer('config_detail_id')->comment('設定詳細番号');
            $table->string('config_value_1', 20)->comment('値1');
            $table->string('config_value_2', 20)->nullable()->comment('値2');
            $table->string('config_value_3', 20)->nullable()->comment('値3');
            $table->integer('rank')->default(1)->comment('順番');
            $table->timestamps();
            $table->softDeletes();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('master_user');
        Schema::dropIfExists('member');
        Schema::dropIfExists('member_invoice');
        Schema::dropIfExists('order');
        Schema::dropIfExists('order_detail');
        Schema::dropIfExists('shipment');
        Schema::dropIfExists('shipment_detail');
        Schema::dropIfExists('autoship');
        Schema::dropIfExists('autoship_detail');
        Schema::dropIfExists('autoship_item');
        Schema::dropIfExists('autoship_item_detail');
        Schema::dropIfExists('document');
        Schema::dropIfExists('update_history');
        Schema::dropIfExists('item');
        Schema::dropIfExists('item_detail');
        Schema::dropIfExists('item_image');
        Schema::dropIfExists('item_attribute');
        Schema::dropIfExists('tci');
        Schema::dropIfExists('master_config');
        Schema::dropIfExists('master_config_detail');
    }
}
