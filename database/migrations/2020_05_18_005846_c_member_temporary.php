<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CMemberTemporary extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('member_temporary');

        Schema::create('member_temporary', function (Blueprint $table) {
            $table->increments('id');
            $table->string('member_id',9)->nullable()->comment('会員番号');
            $table->string('company_name',100)->nullable()->comment('会社名');
            $table->string('first_name',50)->nullable()->comment('姓');
            $table->string('last_name',50)->nullable()->comment('名');
            $table->string('first_kana_name',100)->nullable()->comment('姓(カタカナ)');
            $table->string('last_kana_name',100)->nullable()->comment('名(カタカナ)');
            $table->integer('gender')->default(1)->comment('性別');
            $table->integer('member_division')->default(1)->comment('会員区分');
            $table->string('post',8)->nullable()->comment('郵便番号');
            $table->integer('pref')->default(1)->comment('都道府県');
            $table->string('city',100)->nullable()->comment('市区町村');
            $table->string('addr',100)->nullable()->comment('番地');
            $table->string('building',100)->nullable()->comment('建物名');
            $table->string('mail',100)->nullable()->comment('メールアドレス');
            $table->string('phone',20)->nullable()->comment('電話番号');
            $table->string('password',20)->nullable()->comment('パスワード');
            $table->integer('register_division')->default(1)->comment('登録区分');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('member_temporary');
    }
}
