<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CAutoshipMain extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('autoship', function (Blueprint $table) {
            $table->date('order_create_next_date')->nullable()->after('order_create_date')->comment('次回受注作成日');
            $table->dropColumn('sleep_start_date');
            $table->dropColumn('sleep_end_date');
            $table->dropColumn('discount_rate');
            $table->dropColumn('discount_price');
            $table->dropColumn('unit_price_total');
            $table->dropColumn('commission_total');
            $table->dropColumn('selling_price_total');
            $table->dropColumn('discount_total');
            $table->dropColumn('postage_total');
            $table->dropColumn('shipment_price');
            $table->dropColumn('total');
        });
        Schema::table('autoship_detail', function (Blueprint $table) {
            $table->integer('autoship_fixed_id')->after('autoship_id')->comment('固定ID');
        });

        Schema::dropIfExists('autoship_main');

        Schema::create('autoship_main', function (Blueprint $table) {
            $table->increments('id');
            $table->string('autoship_id',12)->comment('定期番号');
            $table->integer('autoship_fixed_id')->comment('固定ID');
            $table->integer('discount_rate')->comment('割引率');
            $table->integer('discount_price')->default(0)->comment('割引額');
            $table->integer('unit_price_total')->comment('単価価格合計');
            $table->integer('commission_total')->default(0)->comment('コミッション合計');
            $table->integer('selling_price_total')->comment('販売価格合計');
            $table->integer('discount_total')->default(0)->comment('割引額合計');
            $table->integer('postage_total')->default(0)->comment('送料合計');
            $table->integer('shipment_price')->default(0)->comment('代引手数料');
            $table->integer('total')->comment('合計金額');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('autoship_main');
    }
}
