<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterAutoshipRelation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('tci');

        Schema::table('autoship_item', function (Blueprint $table) {
            $table->dropColumn('shipment_price');
        });

        Schema::dropIfExists('autoship_item_image');

        Schema::create('autoship_item_image', function (Blueprint $table) {
            $table->increments('id');
            $table->string('autoship_item_id',12)->comment('商品番号');
            $table->string('file_url',255)->comment('画像URL');
            $table->integer('rank')->comment('順番');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('autoship_item', function (Blueprint $table) {
            $table->integer('shipment_price')->default(0)->after('postage_total')->comment('代引手数料');
        });
        Schema::dropIfExists('autoship_item_image');

    }
}
