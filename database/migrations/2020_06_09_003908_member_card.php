<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class MemberCard extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('member_card');

        Schema::create('member_card', function (Blueprint $table) {
            $table->increments('id');
            $table->string('member_id',9)->comment('会員番号');
            $table->string('credit_number',4)->nullable()->comment('クレジットカード番号');
            $table->string('credit_name',100)->nullable()->comment('クレジットカード名義');
            $table->string('credit_expire_year',2)->nullable()->comment('クレジットカード有効期限年');
            $table->string('credit_expire_month',2)->nullable()->comment('クレジットカード有効期限月');
            $table->string('credit_token',100)->nullable()->comment('クレジットトークン');
        });

        Schema::table('member', function (Blueprint $table) {
            $table->dropColumn('credit_number');
            $table->dropColumn('credit_name');
            $table->dropColumn('credit_expire_year');
            $table->dropColumn('credit_expire_month');
            $table->dropColumn('credit_token');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('member_card');
    }
}
