<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterAutoshipItem extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('autoship_item_interval');

        Schema::create('autoship_item_interval', function (Blueprint $table) {
            $table->increments('id');
            $table->string('autoship_item_id',12)->comment('定期番号');
            $table->integer('shipment_interval')->comment('配送間隔');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('autoship_item_interval');
    }
}
