<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAutoship4 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('autoship', function (Blueprint $table) {
            $table->boolean('order_create_count')->default(1)->after('order_create_next_date')->comment('受注作成回数');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('autoship', function (Blueprint $table) {
            $table->dropColumn('order_create_count');
        });
    }
}
