<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAutoship2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('autoship', function (Blueprint $table) {
            $table->boolean('skip_flag')->default(false)->after('memo')->comment('スキップフラグ');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('autoship', function (Blueprint $table) {
            $table->dropColumn('skip_flag');
        });
    }
}
