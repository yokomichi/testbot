<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CItemStock extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('item_stock');

        Schema::create('item_stock', function (Blueprint $table) {
            $table->increments('id');
            $table->string('item_id',12)->comment('商品番号');
            $table->integer('shipment_origin_division')->comment('発送元区分');
            $table->integer('stock')->comment('在庫');
        });

        Schema::dropIfExists('update_history_item_stock');

        Schema::create('update_history_item_stock', function (Blueprint $table) {
            $table->increments('id');
            $table->string('field_name', 255)->comment('カラム名');
            $table->string('before_value', 255)->nullable()->comment('変更前値');
            $table->string('after_value', 255)->nullable()->comment('変更後値');
            $table->string('user_id', 255)->comment('変更者ID');
            $table->string('user_name', 255)->comment('変更者名');
            $table->timestamps();
            $table->softDeletes();
        });
}

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('item_stock');
        Schema::dropIfExists('update_history_item_stock');
    }
}
