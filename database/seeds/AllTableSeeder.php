<?php

use Illuminate\Database\Seeder;
use \Illuminate\Filesystem\FileNotFoundException;
// use SplFileObject;
// use File;

class AllTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::beginTransaction();
        try {
            DB::table('master_control')->truncate();
            $master_control_data = [
                ['parent_name' => '会員管理', 'name' => '会員登録', 'type' => 'sidemenu', 'upper' => 'member', 'middle' => 'register', 'lower' => null, 'display_status' => true, 'created_at' => now()],
                ['parent_name' => '会員管理', 'name' => '会員検索', 'type' => 'sidemenu', 'upper' => 'member', 'middle' => 'search', 'lower' => null, 'display_status' => true,  'created_at' => now()],
                ['parent_name' => '受注管理', 'name' => '受注登録', 'type' => 'sidemenu', 'upper' => 'order', 'middle' => 'register', 'lower' => null, 'display_status' => true,  'created_at' => now()],
                ['parent_name' => '受注管理', 'name' => '受注検索', 'type' => 'sidemenu', 'upper' => 'order', 'middle' => 'search', 'lower' => null, 'display_status' => true,  'created_at' => now()],
                ['parent_name' => '出荷管理', 'name' => '出荷検索', 'type' => 'sidemenu', 'upper' => 'shipment', 'middle' => 'search', 'lower' => null, 'display_status' => true,  'created_at' => now()],
                ['parent_name' => '定期管理', 'name' => '定期検索', 'type' => 'sidemenu', 'upper' => 'autoship', 'middle' => 'search', 'lower' => null, 'display_status' => true,  'created_at' => now()],
                ['parent_name' => '定期管理', 'name' => '定期商品登録', 'type' => 'sidemenu', 'upper' => 'autoship', 'middle' => 'item', 'lower' => 'register', 'display_status' => true,  'created_at' => now()],
                ['parent_name' => '定期管理', 'name' => '定期商品検索', 'type' => 'sidemenu', 'upper' => 'autoship', 'middle' => 'item', 'lower' => 'search', 'display_status' => true,  'created_at' => now()],
                ['parent_name' => '商品管理', 'name' => '商品登録', 'type' => 'sidemenu', 'upper' => 'item', 'middle' => 'register', 'lower' => null,  'display_status' => true, 'created_at' => now()],
                ['parent_name' => '商品管理', 'name' => '商品検索', 'type' => 'sidemenu', 'upper' => 'item', 'middle' => 'search', 'lower' => null, 'display_status' => true,  'created_at' => now()],
                ['parent_name' => '在庫管理', 'name' => '入出庫登録', 'type' => 'sidemenu', 'upper' => 'stock', 'middle' => 'register', 'lower' => null, 'display_status' => true,  'created_at' => now()],
                ['parent_name' => '在庫管理', 'name' => '入出庫検索', 'type' => 'sidemenu', 'upper' => 'stock', 'middle' => 'search', 'lower' => null, 'display_status' => true,  'created_at' => now()],
                ['parent_name' => '設定管理', 'name' => '設定検索', 'type' => 'sidemenu', 'upper' => 'config', 'middle' => 'search', 'lower' => null, 'display_status' => true, 'created_at' => now()],
                ['parent_name' => '設定管理', 'name' => 'メニュー設定', 'type' => 'sidemenu', 'upper' => 'config', 'middle' => 'menu', 'lower' => null, 'display_status' => true, 'created_at' => now()],
                ['parent_name' => '設定管理', 'name' => '管理ユーザ登録', 'type' => 'sidemenu', 'upper' => 'config', 'middle' => 'user', 'lower' => 'register', 'display_status' => true,  'created_at' => now()],
                ['parent_name' => '設定管理', 'name' => '管理ユーザ検索', 'type' => 'sidemenu', 'upper' => 'config', 'middle' => 'user', 'lower' => 'search', 'display_status' => true,  'created_at' => now()],
            ];
            DB::table('master_control')->insert($master_control_data);

            DB::table('master_user_control')->truncate();
            $master_user_control_data = [];
            for ($i = 1; $i <= 9; ++$i) {
                foreach ($master_control_data as $v) {
                    $master_user_control_data[] = ['name' => $v['name'], 'type' => $v['type'], 'role' => $i, 'upper' => $v['upper'], 'middle' => $v['middle'], 'lower' => $v['lower'], 'display_status' => $i === 9 ? true : false, 'created_at' => now()];
                }
            }
            DB::table('master_user_control')->insert($master_user_control_data);
            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
    }
}
