const path = require('path');
const VueLoaderPlugin = require('vue-loader/lib/plugin');
const MODE = "production";
module.exports = {
    // watch: true,
    mode: MODE,
    // devtool: 'source-map',

    entry: {
        vue: ['@babel/polyfill', './resources/js/app.js'],
        pc_scss: './resources/sass/pc.scss',
        sp_scss: './resources/sass/sp.scss'
    },
    output: {
        filename: 'js/[name].bundle.js',
        path: path.resolve(__dirname, 'public')
    },
    module: {
        rules: [
            {
                test: /\.vue$/,
                use: [
                    {
                        loader: "vue-loader"
                    }
                ]
            },
            {
                test: /\.js$/,
                exclude: /node_modules/,
                loader: 'babel-loader',
                // Babel のオプションを指定する
                options: {
                    presets: [
                        // プリセットを指定することで、ES2019 を ES5 に変換
                        "@babel/preset-env"
                    ]
                }
            },
            {
                test: /\.css$/,
                exclude: /node_modules/,
                loader: ['style-loader', 'css-loader'],
            },
            {
                test: /\.scss$/,
                // use: [
                //   "style-loader", // creates style nodes from JS strings
                //   "css-loader", // translates CSS into CommonJS
                //   "sass-loader" // compiles Sass to CSS, using Node Sass by default
                // ]
                use: [
                    // // linkタグに出力する機能
                    // "style-loader",
                    // {
					// 	loader: 'file-loader',
					// 	options: {
					// 		name: 'css/[name].bundle.css',
					// 	}
					// },

                    // // CSSをバンドルするための機能
                    // {
                    //   loader: "css-loader",
                    //   options: {
                    //     // オプションでCSS内のurl()メソッドを取り込む
                    //     url: false,
                    //   }
                    // },
                    // // Sassをバンドルするための機能
                    // {
                    //   loader: "sass-loader",
                    // }
                    {
						loader: 'file-loader',
						options: {
							name: 'css/[name].bundle.css',
						}
					},
					{
						loader: 'extract-loader'
					},
					{
						loader: 'css-loader?-url'
					},
					{
                        loader: 'postcss-loader',
                        options: {
                            plugins: () => [require('autoprefixer')]
                        }
					},
					{
						loader: 'sass-loader'
                    }
                ]
            },
            {
                test: /\.(gif|png|jpg|eot|wof)$/,
                loader: 'url-loader',
            },
            {
                test: /\.(ttf|eot|woff|woff2|svg)$/,
                use: [{
                    loader: 'file-loader',
                    options: {
                        name: "[name].[ext]",
                        outputPath: './webfonts',
                        publicPath: '../webfonts',
                    }
                }]
            },
        ]
    },
    resolve: {
        alias: {
            'vue$': 'vue/dist/vue.js'
        }
    },
    plugins: [
        new VueLoaderPlugin(),
    ],
    performance: { hints: false }
};